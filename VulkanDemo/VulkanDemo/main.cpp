#include "Device.h"
#include "AssimpLoader.h"
#include <cmath>


// TODO : manage the meshes and the textures with mesh cache and texture cache
void deleteMesh(IVideoDriver* vk, Mesh* mesh)
{
	for (size_t i = 0; i < mesh->getMeshBufferCount(); ++i)
	{
		MeshBuffer* buffer = mesh->getMeshBuffer(i);

		if (buffer->_texture)
		{
			if (vk->deleteTextureBuffer(buffer->_texture))
				delete buffer->_texture;
		}
		vk->deleteMeshBuffer(buffer);
	}
	delete mesh;
}

int main()
{
	Device* device = new Device(Driver_OpenGl, 800, 600, false);
	IVideoDriver* vk = device->getVkVideoDriver();
	SceneManager* sceneManager = device->getSceneManager();
	sceneManager->setAmbiantLight(glm::vec3(0.3f, 0.0f, 0.0f));
	sceneManager->setAmbiantLight(glm::vec3(0.1f, 0.0f, 0.0f));

	// Teapot
	Mesh* meshTeapot = AssimpLoader::loadMesh("wt_teapot.obj");
	for (unsigned int i = 0; i < meshTeapot->getMeshBuffer(0)->_vertices.size(); ++i)
		meshTeapot->getMeshBuffer(0)->_vertices[i].Color = glm::vec3(0.7f, 0.7f, 0.7f);

	MeshSceneNode* nodeTeapot = sceneManager->addMeshNode(meshTeapot);
	nodeTeapot->_lighting = true;
	nodeTeapot->setVisible(false);



	// ship
	
	Mesh* ship = AssimpLoader::loadMesh("ship_JPG_export/ship_JPG.obj");
	for (unsigned int n = 0; n < ship->getMeshBufferCount(); ++n)
		for (unsigned int i = 0; i < ship->getMeshBuffer(n)->_vertices.size(); ++i)
			ship->getMeshBuffer(n)->_vertices[i].Color = glm::vec3(0, 0, 0);
			

	MeshSceneNode* nodeShip = sceneManager->addMeshNode(ship);
	nodeShip->setScale(glm::vec3(0.1f, 0.1f, 0.1f));
	nodeShip->_lighting = true;

	// Dwarf
	Mesh* meshDwarf = AssimpLoader::loadMesh("dwarf.x");
	//meshDwarf->_texture = new Texture("textures/dwarf.jpg");
	for (int i = 0; i < 0; ++i)
	{
		MeshSceneNode* nodeDwarf = sceneManager->addMeshNode(meshDwarf);
		nodeDwarf->setScale(glm::vec3(0.02f, 0.02f, 0.02f));
		nodeDwarf->setRotation(glm::vec3(0.0f, 60.0f, 0.0f));
		nodeDwarf->setPosition(glm::vec3(-1, 0, 0.5f));
		nodeDwarf->_lighting = true;
	}


	sceneManager->_camera->setPosition(glm::vec3(2.0, 2.0, 2.0));

	LightSceneNode* light = sceneManager->addLightNode();
	light->_color = glm::vec4(1.0, 0.0, 0.0, 0.0);
	LightSceneNode* light2 = sceneManager->addLightNode();
	light2->_color = glm::vec4(0.0, 1.0, 0.0, 0.0);
	LightSceneNode* light3 = sceneManager->addLightNode();
	light3->_color = glm::vec4(0.0, 0.0, 1.0, 1.0);

	uint64_t i = 0;
	while (device->update())
	{
		device->setWindowTitle("FPS = " + std::to_string(vk->getFPS()));
		i++;
		float scale = sin(i / 1000.f);
		//nodeDwarf->setRotation(glm::vec3(0, 0, i));
		//nodeDwarf->setPosition(nodeDwarf->getPosition() + nodeDwarf->forward() * 0.001f);
		//nodeDwarf->setScale(glm::vec3(scale * 0.02f, scale * 0.02f, scale * 0.02f));

		//vk->_camera->setRotation(glm::vec3(0.0, i, 0.0));
		//std::cout << i % 360 << std::endl;
		nodeTeapot->setRotation(glm::vec3(0, i / 100.0f, 0));
		nodeShip->setRotation(glm::vec3(0, i / 100.0f, 0));

		float divisor = 500.0f;

		float x = sin(i / divisor) * 1, z = cos(i / divisor) * 1;
		light->setPosition(glm::vec3(x, x, z));

		x = sin((i / divisor) + 2.08f);
		z = cos((i / divisor) + 2.08f);
		light2->setPosition(glm::vec3(x, x, z));

		x = sin((i / divisor) + 4.16f);
		z = cos((i / divisor) + 4.16f);
		light3->setPosition(glm::vec3(-x, x, z));

		vk->prepareScene(glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
		//vk->prepareScene(glm::vec4(0.2f, 0.1f, 0.4f, 1.0f));
		sceneManager->drawAll();
		vk->endScene();
	}

	sceneManager->clear();
	deleteMesh(vk, meshDwarf);
	deleteMesh(vk, meshTeapot);

	delete device;

	return EXIT_SUCCESS;
}