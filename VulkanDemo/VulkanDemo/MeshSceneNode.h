#pragma once
#include "SceneNode.h"
class MeshSceneNode :
	public SceneNode
{
private:
	Mesh* _mesh;

public:
	MeshSceneNode(Mesh * mesh = nullptr);
	virtual ~MeshSceneNode();
	virtual SceneNodeType getNodeType();
	Mesh* getMesh();

	bool _lighting;

#ifdef COMPILE_WITH_VULKAN
	BufferHandle _uniformNodeStagingBuffer;
	BufferHandle _uniformNodeBuffer;

	VkCommandBuffer _copyCommandBuffer = VK_NULL_HANDLE;
#endif
	
};

