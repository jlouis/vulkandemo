#pragma once

#include "SceneManager.h"
#include "VkVideoDriver.h"
#include "OpenGlVideoDriver.h"
#include "Mesh.h"


class Device
{
private:
	GLFWwindow* _window;
	IVideoDriver* _driver;
	SceneManager* _scene;

public:
	Device(DriverType driver, unsigned int width, unsigned int height, bool vsync);
	~Device();
	bool update();
	IVideoDriver* getVkVideoDriver();
	SceneManager* getSceneManager();
	void setWindowTitle(std::string title);
};

