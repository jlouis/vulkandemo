#pragma once
#include "CompileConfig.h"

#ifdef COMPILE_WITH_VULKAN

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>


#define NULL_HANDLE(a) a=VK_NULL_HANDLE

struct BufferHandle
{
	// memory
	VkDeviceMemory _memory;

	// buffer (or image)
	VkBuffer _buffer;
	VkImage _image;

	BufferHandle()
	{
		_memory = VK_NULL_HANDLE;
		_buffer = VK_NULL_HANDLE;
		_image = VK_NULL_HANDLE;
	}

	bool isEmpty()
	{
		if (_buffer == VK_NULL_HANDLE && _memory == VK_NULL_HANDLE)
			return true;
		else if (_image == VK_NULL_HANDLE && _memory == VK_NULL_HANDLE)
			return true;
		else
			return false;
	}

	bool operator!()
	{
		return isEmpty();
	}


	operator VkBuffer()
	{
		if (_buffer != VK_NULL_HANDLE)
			return _buffer;
		else
			return _image;
	}

	
	/*operator VkImage()
	{
		return _image;
	}*/
};

#endif