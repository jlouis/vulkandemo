#include "OpenGlVideoDriver.h"

#ifdef COMPILE_WITH_OGL

GLuint createShader(std::string filename, GLenum shaderType)
{
	// load from file
	std::ifstream file(filename, std::ios::ate);

	if (!file.is_open())
		return 0;

	size_t fileSize = (size_t)file.tellg();
	std::vector<char> buffer(fileSize);

	file.seekg(0);
	file.read(buffer.data(), fileSize);

	const char *vertShaderSrc = buffer.data();
	// create and compile
	GLuint shader = glCreateShader(shaderType);
	glShaderSource(shader, 1, &vertShaderSrc, NULL);
	glCompileShader(shader);

	GLint success;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		GLchar infoLog[512];
		glGetShaderInfoLog(shader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
	}

	return shader;
}

OpenGlVideoDriver::OpenGlVideoDriver(GLFWwindow* window, unsigned int windowWidth, unsigned int windowHeight) : _window(window), _windowWidth(windowWidth), _windowHeight(windowHeight)
{
	//glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
	{
		std::cout << "Failed to initialize GLEW" << std::endl;
		return;
	}

	int width, height;
	glfwGetFramebufferSize(window, &width, &height);

	glViewport(0, 0, width, height);

	GLuint vertexShader = createShader("Shaders/GL/shader.vert", GL_VERTEX_SHADER);
	GLuint fragmentShader = createShader("Shaders/GL/shader.frag", GL_FRAGMENT_SHADER);

	_shaderProgram = glCreateProgram();

	glAttachShader(_shaderProgram, vertexShader);
	glAttachShader(_shaderProgram, fragmentShader);
	glLinkProgram(_shaderProgram);

	GLint success;
	glGetProgramiv(_shaderProgram, GL_LINK_STATUS, &success);
	if (!success) {
		GLchar infoLog[512];
		glGetProgramInfoLog(_shaderProgram, 512, NULL, infoLog);
	}

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	glEnable(GL_DEPTH_TEST);
}

DriverType OpenGlVideoDriver::getType()
{
	return Driver_OpenGl;
}


OpenGlVideoDriver::~OpenGlVideoDriver()
{
}

void OpenGlVideoDriver::prepareScene(glm::vec4 clearColor)
{
	glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void OpenGlVideoDriver::updateSceneUniformBuffer(UniformSceneVertexBuffer& sceneVertexBuffer, UniformSceneFragmentBuffer& sceneFragmentBuffer)
{
	_vertexSceneBuffer = sceneVertexBuffer;
	_vertexSceneBuffer.proj = glm::perspective(glm::radians(45.0f), _windowWidth / (float)_windowHeight, 0.1f, 10.0f);
	_fragmentSceneBuffer = sceneFragmentBuffer;
}

void OpenGlVideoDriver::draw(MeshSceneNode * node)
{
	// Update buffer
	IVideoDriver::draw(node);

	Mesh* mesh = node->getMesh();

	// Draw commands
	glUseProgram(_shaderProgram);

	GLuint uniformLoc = glGetUniformLocation(_shaderProgram, "proj");
	glUniformMatrix4fv(uniformLoc, 1, GL_FALSE, glm::value_ptr(_vertexSceneBuffer.proj));
	uniformLoc = glGetUniformLocation(_shaderProgram, "view");
	glUniformMatrix4fv(uniformLoc, 1, GL_FALSE, glm::value_ptr(_vertexSceneBuffer.view));
	uniformLoc = glGetUniformLocation(_shaderProgram, "model");
	glUniformMatrix4fv(uniformLoc, 1, GL_FALSE, glm::value_ptr(node->getTransformation()));
	uniformLoc = glGetUniformLocation(_shaderProgram, "ambiant");
	glUniform4f(uniformLoc, _fragmentSceneBuffer.ambiant.x, _fragmentSceneBuffer.ambiant.y, _fragmentSceneBuffer.ambiant.z, _fragmentSceneBuffer.ambiant.w);

	uniformLoc = glGetUniformLocation(_shaderProgram, "lighting");
	if (node->_lighting)
		glUniform1f(uniformLoc, 1.0f);
	else
		glUniform1f(uniformLoc, 0.0f);


	uniformLoc = glGetUniformLocation(_shaderProgram, "lightPos");
	glUniform4fv(uniformLoc, NB_MAX_LIGHTS_PER_NODE, (float*)_fragmentSceneBuffer.lightPos);

	uniformLoc = glGetUniformLocation(_shaderProgram, "lightColor");
	glUniform4fv(uniformLoc, NB_MAX_LIGHTS_PER_NODE, (float*)_fragmentSceneBuffer.lightColor);

	for (size_t i = 0; i < mesh->getMeshBufferCount(); ++i)
	{
		MeshBuffer* buffer = mesh->getMeshBuffer(i);

		glBindTexture(GL_TEXTURE_2D, buffer->_texture->_textureBuffer);
		glBindVertexArray(buffer->_glVAOBuffer);
			glDrawElements(GL_TRIANGLES, buffer->_indices.size(), GL_UNSIGNED_SHORT, 0);
		glBindVertexArray(0);
	}

	glUseProgram(0);
}

void OpenGlVideoDriver::endScene()
{
	IVideoDriver::endScene();

	glfwSwapBuffers(_window);
}

void OpenGlVideoDriver::prepareMeshSceneNode(MeshSceneNode* node)
{

}

void OpenGlVideoDriver::deleteMeshSceneNode(MeshSceneNode* node)
{

}

void OpenGlVideoDriver::createMeshBuffer(MeshBuffer* buffer)
{
	std::cout << "BUILD BUFFER" << std::endl;
	deleteMeshBuffer(buffer);

	
	std::vector<float> vertices;
	vertices.reserve(buffer->_vertices.size() * 3);
	for (Vertex vertex : buffer->_vertices)
	{
		vertices.push_back(vertex.Position.x);
		vertices.push_back(vertex.Position.y);
		vertices.push_back(vertex.Position.z);
	}
	



	glGenBuffers(1, &buffer->_glVertexBuffer);
	glGenBuffers(1, &buffer->_glIndicesBuffer);
	glGenVertexArrays(1, &buffer->_glVAOBuffer);
	glBindVertexArray(buffer->_glVAOBuffer);

		glBindBuffer(GL_ARRAY_BUFFER, buffer->_glVertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * buffer->_vertices.size(), buffer->_vertices.data(), GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer->_glIndicesBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint16_t) * buffer->_indices.size(), buffer->_indices.data(), GL_STATIC_DRAW);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(3 * sizeof(GLfloat)));
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(6 * sizeof(GLfloat)));
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(9 * sizeof(GLfloat)));
		glEnableVertexAttribArray(3);

	glBindVertexArray(0);
	buffer->_dirty = false;
}

void OpenGlVideoDriver::deleteMeshBuffer(MeshBuffer* buffer)
{
	if (glIsBuffer(buffer->_glVertexBuffer) == GL_TRUE)
		glDeleteBuffers(1, &buffer->_glVertexBuffer);
	if (glIsBuffer(buffer->_glIndicesBuffer) == GL_TRUE)
		glDeleteBuffers(1, &buffer->_glIndicesBuffer);
	if (glIsBuffer(buffer->_glVAOBuffer) == GL_TRUE)
		glDeleteBuffers(1, &buffer->_glVAOBuffer);
	buffer->_dirty = true;
}

void OpenGlVideoDriver::createTextureBuffer(Texture* texture)
{
	// Clear the previous buffer
	deleteTextureBuffer(texture);

	glGenTextures(1, &texture->_textureBuffer);
	glBindTexture(GL_TEXTURE_2D, texture->_textureBuffer);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texture->getDimensions().x, texture->getDimensions().y, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture->getData());
	glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, 0);
	texture->_dirty = false;
}

bool OpenGlVideoDriver::deleteTextureBuffer(Texture* texture)
{
	if (texture == &_emptyTex)
		return false;

	if (glIsTexture(texture->_textureBuffer))
		glDeleteTextures(1, &texture->_textureBuffer);

	texture->_dirty = true;
	return true;
}

#endif