#pragma once
#include "CompileConfig.h"

#ifdef COMPILE_WITH_VULKAN
	#include "BufferHandle.h"
#endif
#ifdef COMPILE_WITH_OGL
	#include <GL/glew.h>
#endif


#include <iostream>

struct vec2int
{
	uint32_t x, y;
};

class Texture
{
	// CPU buffer
	int _width = 0, _height = 0;
	uint8_t* _data = nullptr;
	void clear();
	void createEmptyTex();

public:
	Texture();
	Texture(std::string filename);
	~Texture();

	bool load(std::string filename);

	int getMemorySize();
	vec2int getDimensions();
	void* getData();

	// Vulkan buffer + image view
#ifdef COMPILE_WITH_VULKAN
	BufferHandle _buffer;
	NULL_HANDLE(VkImageView _textureImageView);
#endif
#ifdef COMPILE_WITH_OGL
	GLuint _textureBuffer;
#endif

	bool _dirty = true;
};

