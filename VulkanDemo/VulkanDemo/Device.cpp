#include "Device.h"

Device::Device(DriverType driver, unsigned int width, unsigned int height, bool vsync)
{
	glfwInit();
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

	if (driver == Driver_OpenGl)
	{
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	}
	else if (driver == Driver_Vulkan)
	{
		glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	}

	_window = glfwCreateWindow(width, height, "VULKAN DEMO", NULL, NULL);

	if (driver == Driver_OpenGl)
	{
		glfwMakeContextCurrent(_window);
		if (!vsync)
			glfwSwapInterval(0);
	}

	_driver = nullptr;
	#ifdef COMPILE_WITH_VULKAN
		if (driver == Driver_Vulkan)
			_driver = new VkVideoDriver(_window, width, height, vsync);
	#endif
	#ifdef COMPILE_WITH_OGL
		if (driver == Driver_OpenGl)
			_driver = new OpenGlVideoDriver(_window, width, height);
	#endif 




	_scene = new SceneManager(_driver);
}

Device::~Device()
{
	delete _scene;
	delete _driver;
	glfwDestroyWindow(_window);
}

bool Device::update()
{
	glfwPollEvents();

	//if (_driver->getType() == Driver_OpenGl)
	//	glfwSwapBuffers(_window);

	return !glfwWindowShouldClose(_window);
}

IVideoDriver * Device::getVkVideoDriver()
{
	return _driver;
}

SceneManager* Device::getSceneManager()
{
	return _scene;
}

void Device::setWindowTitle(std::string title)
{
	glfwSetWindowTitle(_window, title.c_str());
}
