#pragma once
#include "CompileConfig.h"

#ifdef COMPILE_WITH_VULKAN

//#define GLFW_INCLUDE_VULKAN
//#include <GLFW/glfw3.h>

#include <vector>
#include <set>
#include <exception>
#include <algorithm>
#include <array>
#include <fstream>
#include <iostream>
#include <string>

#include "IVideoDriver.h"

// Node data
struct UniformNodeVertexBuffer
{
	glm::mat4 model;
};

struct UniformNodeFragmentBuffer
{
	float lighting;
};


struct QueueFamilyIndices
{
	int graphicsFamily = -1;
	int presentFamily = -1;

	bool isComplete()
	{
		return graphicsFamily >= 0 && presentFamily >= 0;
	}
};

struct SwapChainSupportDetails
{
	VkSurfaceCapabilitiesKHR capabilities;
	std::vector<VkSurfaceFormatKHR> formats;
	std::vector<VkPresentModeKHR> presentModes;
};

class VkVideoDriver : public IVideoDriver
{
private:
	GLFWwindow* _window = nullptr;
	unsigned int _windowWidth = 0;
	unsigned int _windowHeight = 0;

	VkInstance _instance = VK_NULL_HANDLE;
	VkDebugReportCallbackEXT _callback = VK_NULL_HANDLE;
	VkSurfaceKHR _surface = VK_NULL_HANDLE;
	VkPhysicalDevice _physicalDevice = VK_NULL_HANDLE;
	VkDevice _device = VK_NULL_HANDLE;

	VkQueue _graphicQueue = VK_NULL_HANDLE;
	VkQueue _presentationQueue = VK_NULL_HANDLE;

	VkSwapchainKHR _swapChain = VK_NULL_HANDLE;
	VkFormat _swapChainImageFormat;
	VkExtent2D _swapChainExtent;
	std::vector<VkImage> _swapChainImages;
	std::vector<VkImageView> _swapChainImageViews;
	std::vector<VkFramebuffer> _swapChainFramebuffers;
	VkSemaphore _imageAvailableSemaphore = VK_NULL_HANDLE;
	VkSemaphore _renderFinishedSemaphore = VK_NULL_HANDLE;
	VkCommandPool _commandPool = VK_NULL_HANDLE;
	VkRenderPass _renderPass = VK_NULL_HANDLE;
	BufferHandle _depthImage;
	VkImageView _depthImageView = VK_NULL_HANDLE;
	VkPipeline _graphicsPipeline = VK_NULL_HANDLE;
	VkPipelineLayout _pipelineLayout = VK_NULL_HANDLE;
	VkDescriptorSetLayout _descriptorSetLayout = VK_NULL_HANDLE;
	VkDescriptorPool _descriptorPool = VK_NULL_HANDLE;
	VkSampler _textureSampler = VK_NULL_HANDLE;

	BufferHandle _uniformSceneStagingBuffer;
	void* _sceneData;
	BufferHandle _uniformSceneBuffer;
	VkCommandBuffer _copySceneCommandBuffer = VK_NULL_HANDLE;


#ifdef VK_USE_VALIDATION_LAYERS
	const std::vector<const char*> _validationLayers =
	{
		"VK_LAYER_LUNARG_standard_validation"
		//, "VK_LAYER_LUNARG_api_dump"
	};
#else
	const std::vector<const char*> _validationLayers = {};
#endif // VK_USE_VALIDATION_LAYERS


	const std::vector<const char*> _deviceExtensions =
	{
		VK_KHR_SWAPCHAIN_EXTENSION_NAME
	};


	void createVkInstance();
	void createVkDebugCallback();
	void createVkScreenSurface();
	QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device);
	bool checkDeviceExtensionSupport(VkPhysicalDevice device);
	SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device);
	bool isDeviceSuitable(VkPhysicalDevice device);
	void pickPhysicalDevice();
	void createVkLogicalDevice();

	VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats);
	VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR & capabilities);
	void createVkSwapChain(bool vsync);
	void createVkImageViews();
	void createVkRenderPass();
	void createVkDescriptorSetLayout();
	VkDeviceSize getBufferOffset(VkDeviceSize offset);
	void createVkGraphicsPipeline();
	void createVkShaderModule(const std::vector<char>& code, VkShaderModule* shaderModule);
	void createVkFramebuffers();
	void createVkCommandPool();
	void createVkSemaphores();
	void createVkDescriptorPool();
	void createVkTextureSampler();
	void createVkDepthBuffer();
	VkResult destroyVkDebugCallback();

	uint32_t _currentSwapchainImageIndex;
	VkCommandBuffer _drawCommandBuffer = VK_NULL_HANDLE;
	

	// Command buffers utils
	VkCommandBuffer createCommandBuffer(VkCommandBufferUsageFlagBits flags = (VkCommandBufferUsageFlagBits)0x00);
	void submitCommandBuffer(VkCommandBuffer commandBuffer);
	void endSingleTimeCommandBuffer(VkCommandBuffer commandBuffer);

	// Buffer utils
	uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties);
	void createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, BufferHandle& buffer);
	void deleteBuffer(BufferHandle& buffer);
	void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size);

	// Images utils
	void createImage(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties, BufferHandle& buffer);
	void createImageView(VkImage image, VkFormat format, VkImageView & imageView, VkImageAspectFlagBits aspectFlags = VK_IMAGE_ASPECT_COLOR_BIT);
	void transitionImageLayout(BufferHandle image, VkImageLayout oldLayout, VkImageLayout newLayout);
	void copyImage(BufferHandle srcImage, BufferHandle dstImage, uint32_t width, uint32_t height);
	
	// Node utils
	void updateNodeUniformBuffer(MeshSceneNode * node);
	void createNodeMeshBufferDescriptorSet(MeshSceneNode * node, MeshBuffer * buffer);

	void buildBuffers(MeshSceneNode * node);
	

public:
	VkVideoDriver(GLFWwindow* window, unsigned int windowWidth, unsigned int windowHeight, bool vsync);
	~VkVideoDriver();

	virtual DriverType getType();

	void prepareScene(glm::vec4 clearColor);
	void updateSceneUniformBuffer(UniformSceneVertexBuffer& sceneVertexBuffer, UniformSceneFragmentBuffer& sceneFragmentBuffer);
	void draw(MeshSceneNode * node);
	void endScene();

	void prepareMeshSceneNode(MeshSceneNode* node);
	void deleteMeshSceneNode(MeshSceneNode* node);

	void createMeshBuffer(MeshBuffer* buffer);
	void deleteMeshBuffer(MeshBuffer* buffer);

	void createTextureBuffer(Texture* texture);
	bool deleteTextureBuffer(Texture* texture);
};

#endif