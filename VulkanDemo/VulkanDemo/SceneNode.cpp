#include "SceneNode.h"

SceneNode::SceneNode() : 
	_position(0.f, 0.f, 0.f),
	_rotation(0.f, 0.f, 0.f),
	_scale(1.f, 1.f, 1.f),
	_visible(true)
{
}

SceneNode::~SceneNode()
{
}

SceneNodeType SceneNode::getNodeType()
{
	return Node_Other;
}

void SceneNode::setPosition(glm::vec3 position)
{
	_position = position;
}

glm::vec3 SceneNode::getPosition()
{
	return _position;
}

void SceneNode::setRotation(glm::vec3 eulerAngles)
{
	eulerAngles *= (3.14f / 180.0f);
	_rotation = eulerAngles;
}

void SceneNode::setScale(glm::vec3 scale)
{
	_scale = scale;
}

void SceneNode::setVisible(bool visible)
{
	_visible = visible;
}

bool SceneNode::isVisible()
{
	return _visible;
}

glm::vec3 SceneNode::forward()
{
	glm::vec4 forward(0.0, 0.0, 1.0, 0.0);

	glm::mat4x4 transformation;
	const glm::vec3 xAxis(1.0, 0.0, 0.0), yAxis(0.0, 1.0, 0.0), zAxis(0.0, 0.0, 1.0);
	transformation = glm::rotate(transformation, _rotation.x, xAxis);
	transformation = glm::rotate(transformation, _rotation.y, yAxis);
	transformation = glm::rotate(transformation, _rotation.z, zAxis);
	glm::vec4 v = forward * transformation;

	return glm::vec3(v.x, v.y, v.z);
}

glm::mat4x4 SceneNode::getTransformation()
{
	glm::mat4x4 transformation;
	transformation = glm::translate(transformation, _position);

	const glm::vec3 xAxis(1.0, 0.0, 0.0), yAxis(0.0, 1.0, 0.0), zAxis(0.0, 0.0, 1.0);
	transformation = glm::rotate(transformation, _rotation.x, xAxis);
	transformation = glm::rotate(transformation, _rotation.y, yAxis);
	transformation = glm::rotate(transformation, _rotation.z, zAxis);

	transformation = glm::scale(transformation, _scale);
	
	return transformation;
}