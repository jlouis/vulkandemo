#include "MeshBuffer.h"


MeshBuffer::MeshBuffer() : _texture(nullptr)
{
#ifdef COMPILE_WITH_OGL
	_glVertexBuffer = 0;
	_glVAOBuffer = 0;
#endif
}


MeshBuffer::~MeshBuffer()
{
}
