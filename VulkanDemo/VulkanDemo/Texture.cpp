#include "Texture.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"



Texture::Texture()
{
	createEmptyTex();
	_dirty = true;
}

Texture::Texture(std::string filename)
{
	load(filename);
}


Texture::~Texture()
{
	clear();
}

void Texture::clear()
{
	if (_data)
	{
		delete[] _data;
		_data = nullptr;
	}
}

void Texture::createEmptyTex()
{
	clear();

	_width = 1;
	_height = 1;
	_data = new uint8_t[4];
	for (int i = 0; i < 4; ++i)
		_data[i] = 0;
}

bool Texture::load(std::string filename)
{
	clear();

	int texChannels;
	stbi_uc* pixels = stbi_load(filename.c_str(), &_width, &_height, &texChannels, STBI_rgb_alpha);
	if (!pixels)
	{
		std::cout << "Fail to load texture !!! " << std::endl;
		return false;
	}

	const int texSize = getMemorySize();
	_data = new uint8_t[texSize];
	memcpy(_data, pixels, texSize);
	stbi_image_free(pixels);
	return true;
}

int Texture::getMemorySize()
{
	return _width * _height * 4;
}

vec2int Texture::getDimensions()
{
	return{ static_cast<uint32_t>(_width), static_cast<uint32_t>(_height) };
}

void* Texture::getData()
{
	return _data;
}

