#pragma once

#include "Texture.h"



#include <array>
#include <vector>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>



struct Vertex
{
	glm::vec3 Position;
	glm::vec3 Normal;
	glm::vec3 Color;
	glm::vec2 UV;

#ifdef COMPILE_WITH_VULKAN
	static VkVertexInputBindingDescription getBindingDescription() {
		VkVertexInputBindingDescription bindingDescription = {};
		bindingDescription.binding = 0;
		bindingDescription.stride = sizeof(Vertex);
		bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

		return bindingDescription;
	}

	static std::array<VkVertexInputAttributeDescription, 4> getAttributeDescriptions() {
		std::array<VkVertexInputAttributeDescription, 4> attributeDescriptions = {};

		attributeDescriptions[0].binding = 0;
		attributeDescriptions[0].location = 0;
		attributeDescriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;
		attributeDescriptions[0].offset = offsetof(Vertex, Position);

		attributeDescriptions[1].binding = 0;
		attributeDescriptions[1].location = 1;
		attributeDescriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
		attributeDescriptions[1].offset = offsetof(Vertex, Normal);

		attributeDescriptions[2].binding = 0;
		attributeDescriptions[2].location = 2;
		attributeDescriptions[2].format = VK_FORMAT_R32G32B32_SFLOAT;
		attributeDescriptions[2].offset = offsetof(Vertex, Color);

		attributeDescriptions[3].binding = 0;
		attributeDescriptions[3].location = 3;
		attributeDescriptions[3].format = VK_FORMAT_R32G32_SFLOAT;
		attributeDescriptions[3].offset = offsetof(Vertex, UV);

		return attributeDescriptions;
	}
#endif
};


class MeshBuffer
{
private:



public:
	MeshBuffer();
	~MeshBuffer();

	std::vector<Vertex> _vertices;
	std::vector<uint16_t> _indices;

	bool _dirty = true;

	Texture* _texture;

// OpenGL buffers
#ifdef COMPILE_WITH_OGL
	GLuint _glVertexBuffer;
	GLuint _glIndicesBuffer;
	GLuint _glVAOBuffer;
#endif

// Vulkan buffers
#ifdef COMPILE_WITH_VULKAN
	VkDescriptorSet _descriptorSet = VK_NULL_HANDLE;

	BufferHandle _vertexBuffer;
	BufferHandle _indexBuffer;
#endif
	
};

