#include "VkVideoDriver.h"

#ifdef COMPILE_WITH_VULKAN

#define NB_MAX_NODES 100

VkVideoDriver::VkVideoDriver(GLFWwindow* window, unsigned int windowWidth, unsigned int windowHeight, bool vsync) : _window(window), _windowWidth(windowWidth), _windowHeight(windowHeight)
{
	createVkInstance();

#ifdef VK_USE_VALIDATION_LAYERS
	createVkDebugCallback();
#endif // VK_USE_VALIDATION_LAYERS
	
	createVkScreenSurface();
	createVkLogicalDevice();
	createVkSwapChain(vsync);
	createVkImageViews();
	createVkRenderPass();
	createVkDescriptorSetLayout();
	createVkGraphicsPipeline();
	createVkDepthBuffer();
	createVkFramebuffers();
	createVkCommandPool();
	createVkSemaphores();
	createVkDescriptorPool();
	createVkTextureSampler();

	// Create the staging uniform buffers
	VkDeviceSize bufferSize = getBufferOffset(sizeof(UniformSceneVertexBuffer)) + sizeof(UniformSceneFragmentBuffer);
	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, _uniformSceneStagingBuffer);
	vkMapMemory(_device, _uniformSceneStagingBuffer._memory, 0, bufferSize, 0, &_sceneData);

	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, _uniformSceneBuffer);

	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandPool = _commandPool;
	allocInfo.commandBufferCount = 1;

	vkAllocateCommandBuffers(_device, &allocInfo, &_copySceneCommandBuffer);

	VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = 0;

	vkBeginCommandBuffer(_copySceneCommandBuffer, &beginInfo);

	VkBufferCopy copyRegionVertex = {};
	copyRegionVertex.size = getBufferOffset(sizeof(UniformSceneVertexBuffer)) + sizeof(UniformSceneFragmentBuffer);
	vkCmdCopyBuffer(_copySceneCommandBuffer, _uniformSceneStagingBuffer, _uniformSceneBuffer, 1, &copyRegionVertex);

	vkEndCommandBuffer(_copySceneCommandBuffer);
}

VkVideoDriver::~VkVideoDriver()
{
	// Wait
	vkDeviceWaitIdle(_device);

	// And delete
	deleteBuffer(_uniformSceneBuffer);
	deleteBuffer(_uniformSceneStagingBuffer);

	deleteBuffer(_emptyTex._buffer);
	if (_emptyTex._textureImageView != VK_NULL_HANDLE)
		vkDestroyImageView(_device, _emptyTex._textureImageView, nullptr);

	vkDestroySampler(_device, _textureSampler, nullptr);
	vkDestroyDescriptorPool(_device, _descriptorPool, nullptr);

	vkDestroySemaphore(_device, _renderFinishedSemaphore, nullptr);
	vkDestroySemaphore(_device, _imageAvailableSemaphore, nullptr);

	vkDestroyCommandPool(_device, _commandPool, nullptr);

	for (uint32_t i = 0; i < _swapChainImageViews.size(); i++)
		vkDestroyFramebuffer(_device, _swapChainFramebuffers[i], nullptr);

	vkDestroyPipeline(_device, _graphicsPipeline, nullptr);
	vkDestroyRenderPass(_device, _renderPass, nullptr);
	vkDestroyPipelineLayout(_device, _pipelineLayout, nullptr);
	vkDestroyDescriptorSetLayout(_device, _descriptorSetLayout, nullptr);
	
#ifdef VK_USE_VALIDATION_LAYERS
	destroyVkDebugCallback();
#endif // VK_USE_VALIDATION_LAYERS

	for (uint32_t i = 0; i < _swapChainImageViews.size(); i++)
		vkDestroyImageView(_device, _swapChainImageViews[i], nullptr);

	deleteBuffer(_depthImage);
	vkDestroyImageView(_device, _depthImageView, nullptr);

	vkDestroySwapchainKHR(_device, _swapChain, nullptr);
	vkDestroyDevice(_device, nullptr);
	vkDestroySurfaceKHR(_instance, _surface, nullptr);
	vkDestroyInstance(_instance, nullptr);
}

DriverType VkVideoDriver::getType()
{
	return Driver_Vulkan;
}


std::vector<const char*> getRequiredExtensions()
{
	std::vector<const char*> extensions;

	// GLFW extensions
	unsigned int glfwExtensionCount = 0;
	auto glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

	for (unsigned int i = 0; i < glfwExtensionCount; i++)
		extensions.push_back(glfwExtensions[i]);

	// Validation layers report extension
#ifdef VK_USE_VALIDATION_LAYERS
	extensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
#endif // VK_USE_VALIDATION_LAYERS

	return extensions;
}

void VkVideoDriver::createVkInstance()
{
	VkApplicationInfo appInfo = {};
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.apiVersion = VK_API_VERSION_1_0;
	appInfo.pApplicationName = "Vulkan demo";
	appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
	appInfo.pEngineName = "My Vulkan Engine";
	appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);	

	VkInstanceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	createInfo.pApplicationInfo = &appInfo;

	// Get the extensions required to use Vulkan with GLFW
	auto extensionsUsed = getRequiredExtensions();
	createInfo.enabledExtensionCount = extensionsUsed.size();
	createInfo.ppEnabledExtensionNames = extensionsUsed.data();

	// Validation layers
	createInfo.enabledLayerCount = _validationLayers.size();
	createInfo.ppEnabledLayerNames = _validationLayers.data();


	// And create the instance
	VkResult result = vkCreateInstance(&createInfo, nullptr, &_instance);
	if (result != VK_SUCCESS)
		throw std::runtime_error("failed to create instance!");
}

static VkBool32 debugCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objType,
	uint64_t obj, size_t location, int32_t code, const char* layerPrefix, const char* msg, void* userData)
{
	std::string prefix = "[VK_LAYER : ";
	if (flags & VK_DEBUG_REPORT_ERROR_BIT_EXT)
		prefix += "ERROR] ";
	else if (flags & VK_DEBUG_REPORT_WARNING_BIT_EXT)
		prefix += "WARNING] ";
	else if (flags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT)
		prefix += "PERFORMANCE] ";
	else if (flags & VK_DEBUG_REPORT_INFORMATION_BIT_EXT)
		prefix += "INFORMATION] ";

	std::cout << prefix << msg << std::endl;
	return VK_FALSE;
}

void VkVideoDriver::createVkDebugCallback()
{
	VkDebugReportCallbackCreateInfoEXT debugReportCreateInfo = {};
	debugReportCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;

	// print all
	VkDebugReportFlagsEXT flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT | VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT; //| VK_DEBUG_REPORT_INFORMATION_BIT_EXT;
	debugReportCreateInfo.flags = flags;

	debugReportCreateInfo.pfnCallback = (PFN_vkDebugReportCallbackEXT)debugCallback;

	auto func = (PFN_vkCreateDebugReportCallbackEXT)vkGetInstanceProcAddr(_instance, "vkCreateDebugReportCallbackEXT");
	if (func != nullptr)
		func(_instance, &debugReportCreateInfo, nullptr, &_callback);
	else
		throw std::runtime_error("failed to set up debug callback!");
}

VkResult VkVideoDriver::destroyVkDebugCallback()
{
	auto func = (PFN_vkDestroyDebugReportCallbackEXT)vkGetInstanceProcAddr(_instance, "vkDestroyDebugReportCallbackEXT");
	if (func != nullptr)
		func(_instance, _callback, nullptr);
	
	return VK_ERROR_EXTENSION_NOT_PRESENT;
}

void VkVideoDriver::createVkScreenSurface()
{
	if (glfwCreateWindowSurface(_instance, _window, nullptr, &_surface) != VK_SUCCESS)
		throw std::runtime_error("failed to create window surface!");
}


QueueFamilyIndices VkVideoDriver::findQueueFamilies(VkPhysicalDevice device)
{
	QueueFamilyIndices indices;

	uint32_t queueFamilyCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);

	std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

	int i = 0;
	for (const auto& queueFamily : queueFamilies)
	{
		if (queueFamily.queueCount > 0 && queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT)
			indices.graphicsFamily = i;

		VkBool32 presentSupport = false;
		vkGetPhysicalDeviceSurfaceSupportKHR(device, i, _surface, &presentSupport);
		if (queueFamily.queueCount > 0 && presentSupport == VK_TRUE)
			indices.presentFamily = i;

		if (indices.isComplete())
			break;

		i++;
	}

	return indices;
}

bool VkVideoDriver::checkDeviceExtensionSupport(VkPhysicalDevice device)
{
	uint32_t nbExtensionsSupported;
	vkEnumerateDeviceExtensionProperties(device, NULL, &nbExtensionsSupported, NULL);
	std::vector<VkExtensionProperties> extensionsSupported(nbExtensionsSupported);
	vkEnumerateDeviceExtensionProperties(device, NULL, &nbExtensionsSupported, extensionsSupported.data());

	std::set<std::string> requiredExtensions(_deviceExtensions.begin(), _deviceExtensions.end());

	for (const auto& extension : extensionsSupported)
		requiredExtensions.erase(extension.extensionName);

	return requiredExtensions.empty();
}

SwapChainSupportDetails VkVideoDriver::querySwapChainSupport(VkPhysicalDevice device)
{
	SwapChainSupportDetails details;

	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, _surface, &details.capabilities);

	uint32_t formatCount;
	vkGetPhysicalDeviceSurfaceFormatsKHR(device, _surface, &formatCount, nullptr);

	if (formatCount != 0) {
		details.formats.resize(formatCount);
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, _surface, &formatCount, details.formats.data());
	}

	uint32_t presentModeCount;
	vkGetPhysicalDeviceSurfacePresentModesKHR(device, _surface, &presentModeCount, nullptr);

	if (presentModeCount != 0) {
		details.presentModes.resize(presentModeCount);
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, _surface, &presentModeCount, details.presentModes.data());
	}

	return details;
}


bool VkVideoDriver::isDeviceSuitable(VkPhysicalDevice device)
{
	VkPhysicalDeviceProperties deviceProperties;
	vkGetPhysicalDeviceProperties(device, &deviceProperties);

	VkPhysicalDeviceFeatures deviceFeatures;
	vkGetPhysicalDeviceFeatures(device, &deviceFeatures);


	QueueFamilyIndices indices = findQueueFamilies(device);

	bool extensionsSupported = checkDeviceExtensionSupport(device);

	bool swapChainAdequate = false;
	if (extensionsSupported)
	{
		SwapChainSupportDetails swapChainSupport = querySwapChainSupport(device);
		swapChainAdequate = !swapChainSupport.formats.empty() && !swapChainSupport.presentModes.empty();
	}

	return indices.isComplete() && extensionsSupported && swapChainAdequate;
}

void VkVideoDriver::pickPhysicalDevice()
{
	_physicalDevice = VK_NULL_HANDLE;
	uint32_t deviceCount = 0;
	vkEnumeratePhysicalDevices(_instance, &deviceCount, nullptr);

	if (deviceCount == 0)
		throw std::runtime_error("failed to find GPUs with Vulkan support!");

	std::vector<VkPhysicalDevice> devices(deviceCount);
	vkEnumeratePhysicalDevices(_instance, &deviceCount, devices.data());

	for (const auto& device : devices)
		if (isDeviceSuitable(device))
		{
			_physicalDevice = device;
			break;
		}

	if (_physicalDevice == VK_NULL_HANDLE)
		throw std::runtime_error("failed to find a suitable GPU!");
}

void VkVideoDriver::createVkLogicalDevice()
{
	pickPhysicalDevice();

	QueueFamilyIndices indices = findQueueFamilies(_physicalDevice);
	std::set<int> uniqueQueueFamilies = { indices.graphicsFamily, indices.presentFamily };
	std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;

	for (const auto queueFamily : uniqueQueueFamilies)
	{
		VkDeviceQueueCreateInfo queueCreateInfo = {};
		queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo.queueFamilyIndex = indices.graphicsFamily;
		queueCreateInfo.queueCount = 1;

		float queuePriority = 1.0f;
		queueCreateInfo.pQueuePriorities = &queuePriority;
		queueCreateInfos.push_back(queueCreateInfo);
	}

	VkPhysicalDeviceFeatures deviceFeatures = {};
	deviceFeatures.shaderClipDistance = VK_TRUE;
	deviceFeatures.shaderCullDistance = VK_TRUE;

	VkDeviceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	createInfo.pEnabledFeatures = &deviceFeatures;
	createInfo.queueCreateInfoCount = queueCreateInfos.size();
	createInfo.pQueueCreateInfos = queueCreateInfos.data();

	createInfo.enabledExtensionCount = _deviceExtensions.size();
	createInfo.ppEnabledExtensionNames = _deviceExtensions.data();


	createInfo.enabledLayerCount = _validationLayers.size();
	createInfo.ppEnabledLayerNames = _validationLayers.data();
	

	if (vkCreateDevice(_physicalDevice, &createInfo, nullptr, &_device) != VK_SUCCESS)
		throw std::runtime_error("failed to create logical device!");

	vkGetDeviceQueue(_device, indices.graphicsFamily, 0, &_graphicQueue);
	vkGetDeviceQueue(_device, indices.presentFamily, 0, &_presentationQueue);
}


VkSurfaceFormatKHR VkVideoDriver::chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats)
{
	if (availableFormats.size() == 1 && availableFormats[0].format == VK_FORMAT_UNDEFINED) {
		return{ VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
	}

	for (const auto& availableFormat : availableFormats) {
		if (availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
			return availableFormat;
		}
	}

	return availableFormats[0];
}

VkExtent2D VkVideoDriver::chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities)
{
	if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
		return capabilities.currentExtent;
	}
	else {
		VkExtent2D actualExtent = { _windowWidth, _windowHeight };

		actualExtent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actualExtent.width));
		actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));

		return actualExtent;
	}
}


void VkVideoDriver::createVkSwapChain(bool vsync)
{
	SwapChainSupportDetails swapChainSupport = querySwapChainSupport(_physicalDevice);

	VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(swapChainSupport.formats);

	VkPresentModeKHR presentMode;
	if (vsync)
		presentMode = VK_PRESENT_MODE_FIFO_KHR;
	else
		presentMode = VK_PRESENT_MODE_MAILBOX_KHR;

	VkExtent2D extent = chooseSwapExtent(swapChainSupport.capabilities);

	uint32_t imageCount = swapChainSupport.capabilities.minImageCount;

	VkSwapchainCreateInfoKHR createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	createInfo.surface = _surface;

	createInfo.minImageCount = imageCount;
	createInfo.imageFormat = surfaceFormat.format;
	createInfo.imageColorSpace = surfaceFormat.colorSpace;
	createInfo.imageExtent = extent;
	createInfo.imageArrayLayers = 1;
	createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

	QueueFamilyIndices indices = findQueueFamilies(_physicalDevice);
	uint32_t queueFamilyIndices[] = { (uint32_t)indices.graphicsFamily, (uint32_t)indices.presentFamily };

	if (indices.graphicsFamily != indices.presentFamily) {
		createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		createInfo.queueFamilyIndexCount = 2;
		createInfo.pQueueFamilyIndices = queueFamilyIndices;
	}
	else {
		createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
		createInfo.queueFamilyIndexCount = 0; // Optional
		createInfo.pQueueFamilyIndices = nullptr; // Optional
	}

	createInfo.preTransform = swapChainSupport.capabilities.currentTransform;
	createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	createInfo.presentMode = presentMode;
	createInfo.clipped = VK_TRUE;

	VkSwapchainKHR oldSwapChain = _swapChain;
	createInfo.oldSwapchain = _swapChain;

	if (vkCreateSwapchainKHR(_device, &createInfo, nullptr, &_swapChain) != VK_SUCCESS)
		throw std::runtime_error("failed to create swap chain!");

	if (oldSwapChain != VK_NULL_HANDLE)
		vkDestroySwapchainKHR(_device, oldSwapChain, nullptr);


	vkGetSwapchainImagesKHR(_device, _swapChain, &imageCount, nullptr);
	_swapChainImages.resize(imageCount);
	vkGetSwapchainImagesKHR(_device, _swapChain, &imageCount, _swapChainImages.data());

	_swapChainImageFormat = surfaceFormat.format;
	_swapChainExtent = extent;
}


void VkVideoDriver::createVkImageViews()
{
	// Clear the previous framebuffers
	for (size_t i = 0; i < _swapChainImageViews.size(); ++i)
		vkDestroyImageView(_device, _swapChainImageViews[i], nullptr);

	_swapChainImageViews.resize(_swapChainImages.size());
	for (uint32_t i = 0; i < _swapChainImageViews.size(); i++)
		createImageView(_swapChainImages[i], _swapChainImageFormat, _swapChainImageViews[i], VK_IMAGE_ASPECT_COLOR_BIT);
}

void VkVideoDriver::createVkRenderPass()
{
	if (_renderPass != VK_NULL_HANDLE)
		vkDestroyRenderPass(_device, _renderPass, nullptr);

	VkAttachmentDescription colorAttachment = {};
	colorAttachment.format = _swapChainImageFormat;
	colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;

	colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;

	colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;

	colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

	VkAttachmentDescription depthAttachement = {};
	depthAttachement.format = VK_FORMAT_D32_SFLOAT;
	depthAttachement.samples = VK_SAMPLE_COUNT_1_BIT;
	depthAttachement.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	depthAttachement.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	depthAttachement.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	depthAttachement.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	depthAttachement.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	depthAttachement.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;


	VkAttachmentReference colorAttachmentRef = {};
	colorAttachmentRef.attachment = 0;
	colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	VkAttachmentReference depthAttachmentRef = {};
	depthAttachmentRef.attachment = 1;
	depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	VkSubpassDescription subPass = {};
	subPass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subPass.colorAttachmentCount = 1;
	subPass.pDepthStencilAttachment = &depthAttachmentRef;
	subPass.pColorAttachments = &colorAttachmentRef;

	std::array<VkAttachmentDescription, 2> attachments = { colorAttachment, depthAttachement };
	VkRenderPassCreateInfo renderPassInfo = {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassInfo.attachmentCount = attachments.size();
	renderPassInfo.pAttachments = attachments.data();
	renderPassInfo.subpassCount = 1;
	renderPassInfo.pSubpasses = &subPass;

	VkSubpassDependency dependency = {};
	dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
	dependency.dstSubpass = 0;

	dependency.srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
	dependency.srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;

	dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

	renderPassInfo.dependencyCount = 1;
	renderPassInfo.pDependencies = &dependency;

	if (vkCreateRenderPass(_device, &renderPassInfo, nullptr, &_renderPass) != VK_SUCCESS)
		throw std::runtime_error("failed to create render pass!");
}

void VkVideoDriver::createVkDescriptorSetLayout()
{
	if (_descriptorSetLayout != VK_NULL_HANDLE)
		vkDestroyDescriptorSetLayout(_device, _descriptorSetLayout, nullptr);

	VkDescriptorSetLayoutBinding uboVertexSceneLayoutBinding = {};
	uboVertexSceneLayoutBinding.binding = 0;
	uboVertexSceneLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	uboVertexSceneLayoutBinding.descriptorCount = 1;
	uboVertexSceneLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

	VkDescriptorSetLayoutBinding uboVertexNodeLayoutBinding = {};
	uboVertexNodeLayoutBinding.binding = 1;
	uboVertexNodeLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	uboVertexNodeLayoutBinding.descriptorCount = 1;
	uboVertexNodeLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

	VkDescriptorSetLayoutBinding uboSceneFragmentLayoutBinding = {};
	uboSceneFragmentLayoutBinding.binding = 2;
	uboSceneFragmentLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	uboSceneFragmentLayoutBinding.descriptorCount = 1;
	uboSceneFragmentLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

	VkDescriptorSetLayoutBinding uboNodeFragmentLayoutBinding = {};
	uboNodeFragmentLayoutBinding.binding = 3;
	uboNodeFragmentLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	uboNodeFragmentLayoutBinding.descriptorCount = 1;
	uboNodeFragmentLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
	
	VkDescriptorSetLayoutBinding samplerLayoutBinding = {};
	samplerLayoutBinding.binding = 4;
	samplerLayoutBinding.descriptorCount = 1;
	samplerLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	samplerLayoutBinding.pImmutableSamplers = nullptr;
	samplerLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
	

	std::array<VkDescriptorSetLayoutBinding, 5> bindings = { uboVertexSceneLayoutBinding, uboVertexNodeLayoutBinding, uboNodeFragmentLayoutBinding, uboSceneFragmentLayoutBinding, samplerLayoutBinding };
	VkDescriptorSetLayoutCreateInfo layoutInfo = {};
	layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	layoutInfo.bindingCount = bindings.size();
	layoutInfo.pBindings = bindings.data();

	if (vkCreateDescriptorSetLayout(_device, &layoutInfo, nullptr, &_descriptorSetLayout) != VK_SUCCESS)
		throw std::runtime_error("failed to create descriptor set layout!");
}

static std::vector<char> readFile(const std::string& filename)
{
	std::ifstream file(filename, std::ios::ate | std::ios::binary);

	if (!file.is_open()) {
		throw std::runtime_error("failed to open file!");
	}

	size_t fileSize = (size_t)file.tellg();
	std::vector<char> buffer(fileSize);

	file.seekg(0);
	file.read(buffer.data(), fileSize);
	file.close();

	return buffer;
}

VkDeviceSize VkVideoDriver::getBufferOffset(VkDeviceSize offset)
{
	VkPhysicalDeviceProperties properties;
	vkGetPhysicalDeviceProperties(_physicalDevice, &properties);
	VkDeviceSize minOffsetAlignment = properties.limits.minUniformBufferOffsetAlignment;

	if (offset % minOffsetAlignment == 0)
		return offset;

	return offset + minOffsetAlignment - (offset % minOffsetAlignment);
}

void VkVideoDriver::createVkGraphicsPipeline()
{
	if (_graphicsPipeline != VK_NULL_HANDLE)
	{
		vkDestroyPipeline(_device, _graphicsPipeline, nullptr);
		vkDestroyPipelineLayout(_device, _pipelineLayout, nullptr);
	}



	auto vertShaderCode = readFile("shaders/vert.spv");
	auto fragShaderCode = readFile("shaders/frag.spv");

	VkShaderModule vertShaderModule;
	VkShaderModule fragShaderModule;

	createVkShaderModule(vertShaderCode, &vertShaderModule);
	createVkShaderModule(fragShaderCode, &fragShaderModule);

	VkPipelineShaderStageCreateInfo vertShaderStageInfo = {};
	vertShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
	vertShaderStageInfo.module = vertShaderModule;
	vertShaderStageInfo.pName = "main";

	VkPipelineShaderStageCreateInfo fragShaderStageInfo = {};
	fragShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	fragShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
	fragShaderStageInfo.module = fragShaderModule;
	fragShaderStageInfo.pName = "main";



	VkPipelineShaderStageCreateInfo shaderStages[] = { vertShaderStageInfo, fragShaderStageInfo };

	VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
	vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	//vertexInputInfo.vertexBindingDescriptionCount = 0;
	//vertexInputInfo.vertexAttributeDescriptionCount = 0;

	auto bindingDescription = Vertex::getBindingDescription();
	auto attributeDescriptions = Vertex::getAttributeDescriptions();

	vertexInputInfo.vertexBindingDescriptionCount = 1;
	vertexInputInfo.vertexAttributeDescriptionCount = attributeDescriptions.size();
	vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
	vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();



	VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
	inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_LINE_STRIP;
	inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	inputAssembly.primitiveRestartEnable = VK_FALSE;

	VkViewport viewport = {};
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = (float)_swapChainExtent.width;
	viewport.height = (float)_swapChainExtent.height;
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	VkRect2D scissor = {};
	scissor.offset = { 0, 0 };
	scissor.extent = _swapChainExtent;

	VkPipelineViewportStateCreateInfo viewportState = {};
	viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportState.viewportCount = 1;
	viewportState.pViewports = &viewport;
	viewportState.scissorCount = 1;
	viewportState.pScissors = &scissor;

	VkPipelineRasterizationStateCreateInfo rasterizer = {};
	rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterizer.depthClampEnable = VK_FALSE;
	rasterizer.rasterizerDiscardEnable = VK_FALSE;
	rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
	rasterizer.lineWidth = 1.0f;
	rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;


	//rasterizer.frontFace = VK_FRONT_FACE_CLOCKWISE;
	rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;


	rasterizer.depthBiasEnable = VK_FALSE;

	VkPipelineMultisampleStateCreateInfo multisampling = {};
	multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisampling.sampleShadingEnable = VK_FALSE;
	multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

	VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
	colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
	colorBlendAttachment.blendEnable = VK_FALSE;

	VkPipelineColorBlendStateCreateInfo colorBlending = {};
	colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colorBlending.logicOpEnable = VK_FALSE;
	colorBlending.logicOp = VK_LOGIC_OP_COPY;
	colorBlending.attachmentCount = 1;
	colorBlending.pAttachments = &colorBlendAttachment;
	colorBlending.blendConstants[0] = 0.0f;
	colorBlending.blendConstants[1] = 0.0f;
	colorBlending.blendConstants[2] = 0.0f;
	colorBlending.blendConstants[3] = 0.0f;

	VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
	pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	//pipelineLayoutInfo.setLayoutCount = 0;
	pipelineLayoutInfo.setLayoutCount = 1;
	pipelineLayoutInfo.pSetLayouts = &_descriptorSetLayout;

	pipelineLayoutInfo.pushConstantRangeCount = 0;

	if (vkCreatePipelineLayout(_device, &pipelineLayoutInfo, nullptr, &_pipelineLayout) != VK_SUCCESS) {
		throw std::runtime_error("failed to create pipeline layout!");
	}

	VkPipelineDepthStencilStateCreateInfo depthStencil = {};
	depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	depthStencil.depthTestEnable = VK_TRUE;
	depthStencil.depthWriteEnable = VK_TRUE;
	depthStencil.depthBoundsTestEnable = VK_FALSE;
	depthStencil.depthCompareOp = VK_COMPARE_OP_LESS;
	depthStencil.stencilTestEnable = VK_FALSE;

	VkGraphicsPipelineCreateInfo pipelineInfo = {};
	pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	pipelineInfo.stageCount = 2;
	pipelineInfo.pStages = shaderStages;
	pipelineInfo.pVertexInputState = &vertexInputInfo;
	pipelineInfo.pInputAssemblyState = &inputAssembly;
	pipelineInfo.pViewportState = &viewportState;
	pipelineInfo.pRasterizationState = &rasterizer;
	pipelineInfo.pMultisampleState = &multisampling;
	pipelineInfo.pColorBlendState = &colorBlending;
	pipelineInfo.layout = _pipelineLayout;
	pipelineInfo.renderPass = _renderPass;
	pipelineInfo.subpass = 0;
	pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;
	pipelineInfo.pDepthStencilState = &depthStencil;

	if (vkCreateGraphicsPipelines(_device, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &_graphicsPipeline) != VK_SUCCESS) {
		throw std::runtime_error("failed to create graphics pipeline!");
	}



	vkDestroyShaderModule(_device, vertShaderModule, nullptr);
	vkDestroyShaderModule(_device, fragShaderModule, nullptr);
}

void VkVideoDriver::createVkDepthBuffer()
{
	VkFormat depthFormat = VK_FORMAT_D32_SFLOAT;
	createImage(_swapChainExtent.width, _swapChainExtent.height, depthFormat, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, _depthImage);
	createImageView(_depthImage, depthFormat, _depthImageView, VK_IMAGE_ASPECT_DEPTH_BIT);
}

void VkVideoDriver::createVkShaderModule(const std::vector<char>& code, VkShaderModule* shaderModule)
{
	VkShaderModuleCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	createInfo.codeSize = code.size();
	createInfo.pCode = (uint32_t*)code.data();

	if (vkCreateShaderModule(_device, &createInfo, nullptr, shaderModule) != VK_SUCCESS) {
		throw std::runtime_error("failed to create shader module!");
	}
}

void VkVideoDriver::createVkFramebuffers()
{
	// Clear the previous framebuffers
	for (size_t i = 0; i < _swapChainFramebuffers.size(); ++i)
		vkDestroyFramebuffer(_device, _swapChainFramebuffers[i], nullptr);

	_swapChainFramebuffers.resize(_swapChainImageViews.size());
	for (size_t i = 0; i < _swapChainImageViews.size(); ++i)
	{
		VkImageView attachments[] = {
			_swapChainImageViews[i], _depthImageView
		};

		VkFramebufferCreateInfo framebufferInfo = {};
		framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		framebufferInfo.renderPass = _renderPass;
		framebufferInfo.attachmentCount = 2;
		framebufferInfo.pAttachments = attachments;
		framebufferInfo.width = _swapChainExtent.width;
		framebufferInfo.height = _swapChainExtent.height;
		framebufferInfo.layers = 1;

		if (vkCreateFramebuffer(_device, &framebufferInfo, nullptr, &_swapChainFramebuffers[i]) != VK_SUCCESS) {
			throw std::runtime_error("failed to create framebuffer!");
		}
	}
}

void VkVideoDriver::createVkCommandPool()
{
	VkCommandPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = findQueueFamilies(_physicalDevice).graphicsFamily;
	poolInfo.flags = 0; // Optional

	if (vkCreateCommandPool(_device, &poolInfo, nullptr, &_commandPool) != VK_SUCCESS) {
		throw std::runtime_error("failed to create command pool!");
	}
}

void VkVideoDriver::createVkSemaphores()
{
	VkSemaphoreCreateInfo semaphoreInfo = {};
	semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	if (vkCreateSemaphore(_device, &semaphoreInfo, nullptr, &_imageAvailableSemaphore) != VK_SUCCESS ||
		vkCreateSemaphore(_device, &semaphoreInfo, nullptr, &_renderFinishedSemaphore) != VK_SUCCESS)
		throw std::runtime_error("failed to create semaphores!");
}

void VkVideoDriver::createVkDescriptorPool()
{
	std::array<VkDescriptorPoolSize, 2> poolSizes = {};
	poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	poolSizes[0].descriptorCount = NB_MAX_NODES + 1;
	poolSizes[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	poolSizes[1].descriptorCount = NB_MAX_NODES;


	VkDescriptorPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolInfo.poolSizeCount = poolSizes.size();
	poolInfo.pPoolSizes = poolSizes.data();
	poolInfo.maxSets = NB_MAX_NODES * 2 + 1;

	if (vkCreateDescriptorPool(_device, &poolInfo, nullptr, &_descriptorPool) != VK_SUCCESS)
		throw std::runtime_error("failed to create descriptor pool!");
}

void VkVideoDriver::createVkTextureSampler()
{
	VkSamplerCreateInfo samplerInfo = {};
	samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	samplerInfo.magFilter = VK_FILTER_LINEAR;
	samplerInfo.minFilter = VK_FILTER_LINEAR;
	samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerInfo.anisotropyEnable = VK_TRUE;
	samplerInfo.maxAnisotropy = 16;
	samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
	samplerInfo.unnormalizedCoordinates = VK_FALSE;
	samplerInfo.compareEnable = VK_FALSE;
	samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
	samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;

	if (vkCreateSampler(_device, &samplerInfo, nullptr, &_textureSampler) != VK_SUCCESS)
		throw std::runtime_error("failed to create texture sampler!");
}


void VkVideoDriver::deleteBuffer(BufferHandle& buffer)
{
	if (buffer.isEmpty())
		return;

	if (buffer._memory != VK_NULL_HANDLE)
	{
		vkFreeMemory(_device, buffer._memory, nullptr);
		buffer._memory = VK_NULL_HANDLE;
	}
	if (buffer._buffer != VK_NULL_HANDLE)
	{
		vkDestroyBuffer(_device, buffer._buffer, nullptr);
		buffer._buffer = VK_NULL_HANDLE;
	}
	if (buffer._image != VK_NULL_HANDLE)
	{
		vkDestroyImage(_device, buffer._image, nullptr);
		buffer._image = VK_NULL_HANDLE;
	}
}

uint32_t VkVideoDriver::findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties)
{
	VkPhysicalDeviceMemoryProperties memProperties;
	vkGetPhysicalDeviceMemoryProperties(_physicalDevice, &memProperties);

	for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
		if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
			return i;
		}
	}

	throw std::runtime_error("failed to find suitable memory type!");
}

void VkVideoDriver::createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, BufferHandle& buffer)
{
	VkBufferCreateInfo bufferInfo = {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = size;
	bufferInfo.usage = usage;
	bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	if (vkCreateBuffer(_device, &bufferInfo, nullptr, &buffer._buffer) != VK_SUCCESS) {
		throw std::runtime_error("failed to create buffer!");
	}

	VkMemoryRequirements memRequirements;
	vkGetBufferMemoryRequirements(_device, buffer._buffer, &memRequirements);

	VkMemoryAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocInfo.allocationSize = memRequirements.size;
	allocInfo.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits, properties);

	if (vkAllocateMemory(_device, &allocInfo, nullptr, &buffer._memory) != VK_SUCCESS)
		throw std::runtime_error("failed to allocate buffer memory!");

	vkBindBufferMemory(_device, buffer._buffer, buffer._memory, 0);
}

VkCommandBuffer VkVideoDriver::createCommandBuffer(VkCommandBufferUsageFlagBits flags)
{
	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandPool = _commandPool;
	allocInfo.commandBufferCount = 1;

	VkCommandBuffer commandBuffer;
	vkAllocateCommandBuffers(_device, &allocInfo, &commandBuffer);

	VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = flags;

	vkBeginCommandBuffer(commandBuffer, &beginInfo);

	return commandBuffer;
}

void VkVideoDriver::submitCommandBuffer(VkCommandBuffer commandBuffer)
{
	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffer;

	vkQueueSubmit(_graphicQueue, 1, &submitInfo, VK_NULL_HANDLE);
}

void VkVideoDriver::endSingleTimeCommandBuffer(VkCommandBuffer commandBuffer)
{
	vkEndCommandBuffer(commandBuffer);

	submitCommandBuffer(commandBuffer);

	vkQueueWaitIdle(_graphicQueue);

	vkFreeCommandBuffers(_device, _commandPool, 1, &commandBuffer);
}

void VkVideoDriver::copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size)
{
	VkCommandBuffer commandBuffer = createCommandBuffer(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);

	VkBufferCopy copyRegion = {};
	copyRegion.size = size;
	vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);

	endSingleTimeCommandBuffer(commandBuffer);
}

void VkVideoDriver::createMeshBuffer(MeshBuffer* buffer)
{
	// Clear the previous buffer
	deleteMeshBuffer(buffer);

	// Fill the staging buffer and copy in the vertex buffer
	VkDeviceSize bufferSize = sizeof(Vertex) * buffer->_vertices.size();
	BufferHandle stagingBuffer;
	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer);

	void* data;
	vkMapMemory(_device, stagingBuffer._memory, 0, bufferSize, 0, &data);
	memcpy(data, buffer->_vertices.data(), (size_t)bufferSize);
	vkUnmapMemory(_device, stagingBuffer._memory);

	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, buffer->_vertexBuffer);
	copyBuffer(stagingBuffer, buffer->_vertexBuffer, bufferSize);
	deleteBuffer(stagingBuffer);


	// Indices buffer
	bufferSize = 2 * buffer->_indices.size();
	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer);

	vkMapMemory(_device, stagingBuffer._memory, 0, bufferSize, 0, &data);
	memcpy(data, buffer->_indices.data(), (size_t)bufferSize);
	vkUnmapMemory(_device, stagingBuffer._memory);

	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, buffer->_indexBuffer);
	copyBuffer(stagingBuffer, buffer->_indexBuffer, bufferSize);
	deleteBuffer(stagingBuffer);

	buffer->_dirty = false;
}

void VkVideoDriver::deleteMeshBuffer(MeshBuffer* buffer)
{
	deleteBuffer(buffer->_vertexBuffer);
	deleteBuffer(buffer->_indexBuffer);
	buffer->_dirty = true;
}

void VkVideoDriver::createNodeMeshBufferDescriptorSet(MeshSceneNode* node, MeshBuffer* buffer)
{
	if (buffer->_descriptorSet == VK_NULL_HANDLE)
	{
		VkDescriptorSetLayout layouts[] = { _descriptorSetLayout };
		VkDescriptorSetAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		allocInfo.descriptorPool = _descriptorPool;
		allocInfo.descriptorSetCount = 1;
		allocInfo.pSetLayouts = layouts;

		if (vkAllocateDescriptorSets(_device, &allocInfo, &buffer->_descriptorSet) != VK_SUCCESS)
			throw std::runtime_error("failed to allocate descriptor set!");
	}

	// Scene buffers
	VkDescriptorBufferInfo bufferSceneVertexInfo = {};
	bufferSceneVertexInfo.buffer = _uniformSceneBuffer;
	bufferSceneVertexInfo.offset = 0;
	bufferSceneVertexInfo.range = sizeof(UniformSceneVertexBuffer);

	VkDescriptorBufferInfo bufferSceneFragmentInfo = {};
	bufferSceneFragmentInfo.buffer = _uniformSceneBuffer;
	bufferSceneFragmentInfo.offset = getBufferOffset(sizeof(UniformSceneVertexBuffer));
	bufferSceneFragmentInfo.range = sizeof(UniformNodeFragmentBuffer);

	// Node buffers
	VkDescriptorBufferInfo bufferNodeVertexInfo = {};
	bufferNodeVertexInfo.buffer = node->_uniformNodeBuffer;
	bufferNodeVertexInfo.offset = 0;
	bufferNodeVertexInfo.range = sizeof(UniformNodeVertexBuffer);

	VkDescriptorBufferInfo bufferNodeFragmentInfo = {};
	bufferNodeFragmentInfo.buffer = node->_uniformNodeBuffer;
	bufferNodeFragmentInfo.offset = getBufferOffset(sizeof(UniformNodeVertexBuffer));
	bufferNodeFragmentInfo.range = sizeof(UniformNodeFragmentBuffer);

	// Tex buffer
	VkDescriptorImageInfo imageInfo = {};
	imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	imageInfo.imageView = buffer->_texture->_textureImageView;
	imageInfo.sampler = _textureSampler;



	std::array<VkWriteDescriptorSet, 5> descriptorWrites = {};

	descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	descriptorWrites[0].dstSet = buffer->_descriptorSet;
	descriptorWrites[0].dstBinding = 0;
	descriptorWrites[0].dstArrayElement = 0;
	descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	descriptorWrites[0].descriptorCount = 1;
	descriptorWrites[0].pBufferInfo = &bufferSceneVertexInfo;

	descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	descriptorWrites[1].dstSet = buffer->_descriptorSet;
	descriptorWrites[1].dstBinding = 1;
	descriptorWrites[1].dstArrayElement = 0;
	descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	descriptorWrites[1].descriptorCount = 1;
	descriptorWrites[1].pBufferInfo = &bufferNodeVertexInfo;

	descriptorWrites[2].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	descriptorWrites[2].dstSet = buffer->_descriptorSet;
	descriptorWrites[2].dstBinding = 2;
	descriptorWrites[2].dstArrayElement = 0;
	descriptorWrites[2].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	descriptorWrites[2].descriptorCount = 1;
	descriptorWrites[2].pBufferInfo = &bufferSceneFragmentInfo;

	descriptorWrites[3].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	descriptorWrites[3].dstSet = buffer->_descriptorSet;
	descriptorWrites[3].dstBinding = 3;
	descriptorWrites[3].dstArrayElement = 0;
	descriptorWrites[3].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	descriptorWrites[3].descriptorCount = 1;
	descriptorWrites[3].pBufferInfo = &bufferNodeFragmentInfo;

	descriptorWrites[4].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	descriptorWrites[4].dstSet = buffer->_descriptorSet;
	descriptorWrites[4].dstBinding = 4;
	descriptorWrites[4].dstArrayElement = 0;
	descriptorWrites[4].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	descriptorWrites[4].descriptorCount = 1;
	descriptorWrites[4].pImageInfo = &imageInfo;

	vkUpdateDescriptorSets(_device, descriptorWrites.size(), descriptorWrites.data(), 0, nullptr);
}

void VkVideoDriver::prepareScene(glm::vec4 clearColor)
{
	// Get image from the swap chain
	VkResult result = vkAcquireNextImageKHR(_device, _swapChain, std::numeric_limits<uint64_t>::max(), _imageAvailableSemaphore, VK_NULL_HANDLE, &_currentSwapchainImageIndex);
	if (result != VK_SUCCESS)
		throw std::runtime_error("failed to acquire swap chain image!");


	// Start to record the command
	_drawCommandBuffer = createCommandBuffer(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);

	// Start render pass
	VkRenderPassBeginInfo renderPassInfo = {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderPassInfo.renderPass = _renderPass;
	renderPassInfo.framebuffer = _swapChainFramebuffers[_currentSwapchainImageIndex];
	renderPassInfo.renderArea.offset = { 0, 0 };
	renderPassInfo.renderArea.extent = _swapChainExtent;

	VkClearValue vkClearColor = { clearColor.r, clearColor.g, clearColor.b, clearColor.a };
	VkClearValue clearDepthColor = { 1.0f, 0 };
	std::array<VkClearValue, 2> clearValues = { vkClearColor , clearDepthColor };

	renderPassInfo.clearValueCount = 2;
	renderPassInfo.pClearValues = clearValues.data();

	vkCmdBeginRenderPass(_drawCommandBuffer, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

	// Bind the graphic pipeline
	vkCmdBindPipeline(_drawCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, _graphicsPipeline);
}

void VkVideoDriver::draw(MeshSceneNode* node)
{
	IVideoDriver::draw(node);

	if (!node->isVisible())
		return;

	// Update the buffers
	updateNodeUniformBuffer(node);

	Mesh* mesh = node->getMesh();
	for (size_t i = 0; i < mesh->getMeshBufferCount(); ++i)
	{
		MeshBuffer* buffer = mesh->getMeshBuffer(i);
		if (buffer->_dirty)
			createNodeMeshBufferDescriptorSet(node, buffer);
	}
			

	// Draw commands
	for (size_t i = 0; i < mesh->getMeshBufferCount(); ++i)
	{
		MeshBuffer* buffer = mesh->getMeshBuffer(i);

		VkBuffer vertexBuffers[] = { buffer->_vertexBuffer._buffer };
		VkDeviceSize offsets[] = { 0 };
		vkCmdBindVertexBuffers(_drawCommandBuffer, 0, 1, vertexBuffers, offsets);

		vkCmdBindIndexBuffer(_drawCommandBuffer, buffer->_indexBuffer._buffer, 0, VK_INDEX_TYPE_UINT16);

		vkCmdBindDescriptorSets(_drawCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, _pipelineLayout, 0, 1, &buffer->_descriptorSet, 0, nullptr);

		vkCmdDrawIndexed(_drawCommandBuffer, buffer->_indices.size(), 1, 0, 0, 0);
	}
}

void VkVideoDriver::endScene()
{
	IVideoDriver::endScene();

	// End render pass
	vkCmdEndRenderPass(_drawCommandBuffer);

	// Commands are ready
	vkEndCommandBuffer(_drawCommandBuffer);

	// Submit the draw commands
	VkSubmitInfo submitDrawInfos = {};
	submitDrawInfos.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitDrawInfos.pCommandBuffers = &_drawCommandBuffer;
	submitDrawInfos.commandBufferCount = 1;

	VkSemaphore signalSemaphores[] = { _renderFinishedSemaphore };
	submitDrawInfos.pSignalSemaphores = signalSemaphores;
	submitDrawInfos.signalSemaphoreCount = 1;

	VkSemaphore waitSemaphores[] = { _imageAvailableSemaphore };
	submitDrawInfos.pWaitSemaphores = waitSemaphores;
	submitDrawInfos.waitSemaphoreCount = 1;

	VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
	submitDrawInfos.pWaitDstStageMask = waitStages;


	vkQueueSubmit(_graphicQueue, 1, &submitDrawInfos, VK_NULL_HANDLE);
	vkQueueWaitIdle(_graphicQueue);

	VkCommandBuffer commands[1] = { _drawCommandBuffer };
	vkFreeCommandBuffers(_device, _commandPool, 1, commands);


	// Send the image to the swap chain
	VkPresentInfoKHR presentInfo = {};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

	VkSemaphore presentationWaitSemaphores[] = { _renderFinishedSemaphore };
	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores = presentationWaitSemaphores;

	VkSwapchainKHR swapChains[] = { _swapChain };
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = swapChains;
	presentInfo.pImageIndices = &_currentSwapchainImageIndex;
	vkQueuePresentKHR(_presentationQueue, &presentInfo);
}

void VkVideoDriver::prepareMeshSceneNode(MeshSceneNode* node)
{
	// Create the unifom buffer
	buildBuffers(node);
	
	Mesh* mesh = node->getMesh();
	for (size_t i = 0; i < mesh->getMeshBufferCount(); ++i)
	{
		MeshBuffer* buffer = mesh->getMeshBuffer(i);

		if (!buffer->_texture)
			buffer->_texture = &_emptyTex;

		if (buffer->_texture->_dirty)
			createTextureBuffer(buffer->_texture);

		createNodeMeshBufferDescriptorSet(node, buffer);
	}
}

void VkVideoDriver::deleteMeshSceneNode(MeshSceneNode* node)
{
	deleteBuffer(node->_uniformNodeStagingBuffer);
	deleteBuffer(node->_uniformNodeBuffer);
}

void VkVideoDriver::createImage(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties, BufferHandle& buffer)
{
	VkImageCreateInfo imageInfo = {};
	imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	imageInfo.imageType = VK_IMAGE_TYPE_2D;
	imageInfo.extent.width = width;
	imageInfo.extent.height = height;
	imageInfo.extent.depth = 1;
	imageInfo.mipLevels = 1;
	imageInfo.arrayLayers = 1;
	imageInfo.format = format;
	imageInfo.tiling = tiling;
	imageInfo.initialLayout = VK_IMAGE_LAYOUT_PREINITIALIZED;
	imageInfo.usage = usage;
	imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
	imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	if (vkCreateImage(_device, &imageInfo, nullptr, &buffer._image) != VK_SUCCESS) {
		throw std::runtime_error("failed to create image!");
	}

	VkMemoryRequirements memRequirements;
	vkGetImageMemoryRequirements(_device, buffer._image, &memRequirements);

	VkMemoryAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocInfo.allocationSize = memRequirements.size;
	allocInfo.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits, properties);

	if (vkAllocateMemory(_device, &allocInfo, nullptr, &buffer._memory) != VK_SUCCESS) {
		throw std::runtime_error("failed to allocate image memory!");
	}

	vkBindImageMemory(_device, buffer._image, buffer._memory, 0);
}

void VkVideoDriver::createImageView(VkImage image, VkFormat format, VkImageView& imageView, VkImageAspectFlagBits aspectFlags)
{
	VkImageViewCreateInfo viewInfo = {};
	viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	viewInfo.image = image;
	viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
	viewInfo.format = format;
	viewInfo.subresourceRange.aspectMask = aspectFlags;
	viewInfo.subresourceRange.baseMipLevel = 0;
	viewInfo.subresourceRange.levelCount = 1;
	viewInfo.subresourceRange.baseArrayLayer = 0;
	viewInfo.subresourceRange.layerCount = 1;

	if (vkCreateImageView(_device, &viewInfo, nullptr, &imageView) != VK_SUCCESS)
		throw std::runtime_error("failed to create texture image view!");

}


void VkVideoDriver::transitionImageLayout(BufferHandle image, VkImageLayout oldLayout, VkImageLayout newLayout)
{
	VkCommandBuffer commandBuffer = createCommandBuffer(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);

	VkImageMemoryBarrier barrier = {};
	barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	barrier.oldLayout = oldLayout;
	barrier.newLayout = newLayout;

	barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

	barrier.image = image._image;
	barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	barrier.subresourceRange.baseMipLevel = 0;
	barrier.subresourceRange.levelCount = 1;
	barrier.subresourceRange.baseArrayLayer = 0;
	barrier.subresourceRange.layerCount = 1;

	if (oldLayout == VK_IMAGE_LAYOUT_PREINITIALIZED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL) {
		barrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT;
		barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
	}
	else if (oldLayout == VK_IMAGE_LAYOUT_PREINITIALIZED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
		barrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT;
		barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
	}
	else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
		barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
	}
	else {
		throw std::invalid_argument("unsupported layout transition!");
	}

	vkCmdPipelineBarrier(
		commandBuffer,
		VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
		0,
		0, nullptr,
		0, nullptr,
		1, &barrier
	);

	endSingleTimeCommandBuffer(commandBuffer);
}

void VkVideoDriver::copyImage(BufferHandle srcImage, BufferHandle dstImage, uint32_t width, uint32_t height)
{
	VkCommandBuffer commandBuffer = createCommandBuffer(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);

	VkImageSubresourceLayers subResource = {};
	subResource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	subResource.baseArrayLayer = 0;
	subResource.mipLevel = 0;
	subResource.layerCount = 1;

	VkImageCopy region = {};
	region.srcSubresource = subResource;
	region.dstSubresource = subResource;
	region.srcOffset = { 0, 0, 0 };
	region.dstOffset = { 0, 0, 0 };
	region.extent.width = width;
	region.extent.height = height;
	region.extent.depth = 1;

	vkCmdCopyImage(
		commandBuffer,
		srcImage._image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
		dstImage._image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
		1, &region
	);

	endSingleTimeCommandBuffer(commandBuffer);
}

void VkVideoDriver::createTextureBuffer(Texture* texture)
{
	// Clear the previous buffer
	deleteTextureBuffer(texture);

	BufferHandle stagingImage;
	createImage(texture->getDimensions().x, texture->getDimensions().y, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TILING_LINEAR, VK_IMAGE_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingImage);

	void* data;
	vkMapMemory(_device, stagingImage._memory, 0, texture->getMemorySize(), 0, &data);
	memcpy(data, texture->getData(), (size_t)texture->getMemorySize());
	vkUnmapMemory(_device, stagingImage._memory);

	
	createImage(texture->getDimensions().x, texture->getDimensions().y, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, texture->_buffer);

	transitionImageLayout(stagingImage, VK_IMAGE_LAYOUT_PREINITIALIZED, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);
	transitionImageLayout(texture->_buffer, VK_IMAGE_LAYOUT_PREINITIALIZED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
	copyImage(stagingImage, texture->_buffer, texture->getDimensions().x, texture->getDimensions().y);
	transitionImageLayout(texture->_buffer, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

	createImageView(texture->_buffer, VK_FORMAT_R8G8B8A8_UNORM, texture->_textureImageView);

	deleteBuffer(stagingImage);

	texture->_dirty = false;
}

bool VkVideoDriver::deleteTextureBuffer(Texture* texture)
{
	if (texture == &_emptyTex)
		return false;

	deleteBuffer(texture->_buffer);
	if (texture->_textureImageView != VK_NULL_HANDLE)
		vkDestroyImageView(_device, texture->_textureImageView, nullptr);

	texture->_dirty = true;
	return true;
}

void VkVideoDriver::updateSceneUniformBuffer(UniformSceneVertexBuffer& sceneVertexBuffer, UniformSceneFragmentBuffer& sceneFragmentBuffer)
{
	sceneVertexBuffer.proj = glm::perspective(glm::radians(45.0f), _swapChainExtent.width / (float)_swapChainExtent.height, 0.1f, 10.0f);
	sceneVertexBuffer.proj[1][1] *= -1;

	memcpy(_sceneData, &sceneVertexBuffer, sizeof(UniformSceneVertexBuffer));
	memcpy(static_cast<char*>(_sceneData) + getBufferOffset(sizeof(UniformSceneVertexBuffer)), &sceneFragmentBuffer, sizeof(UniformSceneFragmentBuffer));

	// Copy the buffer
	submitCommandBuffer(_copySceneCommandBuffer);
}

void VkVideoDriver::buildBuffers(MeshSceneNode* node)
{
	VkDeviceSize bufferSize = getBufferOffset(sizeof(UniformNodeVertexBuffer)) + sizeof(UniformNodeFragmentBuffer);
	// no staging buffer
	//createBuffer(bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, node->_uniformNodeStagingBuffer);

	// staging + copy
	
	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, node->_uniformNodeStagingBuffer);
	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, node->_uniformNodeBuffer);

	if (node->_copyCommandBuffer != VK_NULL_HANDLE)
		vkFreeCommandBuffers(_device, _commandPool, 1, &node->_copyCommandBuffer);


	node->_copyCommandBuffer = createCommandBuffer();

	VkBufferCopy copyRegion = {};
	copyRegion.size = getBufferOffset(sizeof(UniformNodeVertexBuffer)) + sizeof(UniformNodeVertexBuffer);
	vkCmdCopyBuffer(node->_copyCommandBuffer, node->_uniformNodeStagingBuffer, node->_uniformNodeBuffer, 1, &copyRegion);

	vkEndCommandBuffer(node->_copyCommandBuffer);
	
}


void VkVideoDriver::updateNodeUniformBuffer(MeshSceneNode* node)
{
	// If the buffer isn't allocated
	if (!node->_uniformNodeBuffer)
		buildBuffers(node);

	// Vertex buffer
	UniformNodeVertexBuffer nodeVertexData;
	nodeVertexData.model = node->getTransformation();

	// Fragment buffer
	UniformNodeFragmentBuffer nodeFragmentData;
	if (node->_lighting)
		nodeFragmentData.lighting = 1.0f;
	else
		nodeFragmentData.lighting = 0.0f;

	// update the staging buffer
	void* data;
	vkMapMemory(_device, node->_uniformNodeStagingBuffer._memory, 0, getBufferOffset(sizeof(UniformNodeVertexBuffer)) + sizeof(UniformNodeFragmentBuffer), 0, &data);
	memcpy(data, &nodeVertexData, sizeof(UniformNodeVertexBuffer));
	memcpy(static_cast<char*>(data) + getBufferOffset(sizeof(UniformNodeVertexBuffer)), &nodeFragmentData, sizeof(UniformNodeFragmentBuffer));
	vkUnmapMemory(_device, node->_uniformNodeStagingBuffer._memory);

	// Copy the buffer
	submitCommandBuffer(node->_copyCommandBuffer);
}

#endif