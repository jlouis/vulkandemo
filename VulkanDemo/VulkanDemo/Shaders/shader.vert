#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inColor;
layout(location = 3) in vec2 inTexCoord;

layout(binding = 0) uniform UniformSceneBuffer {
    mat4 view;
    mat4 proj;
} scene;

layout(binding = 1) uniform UniformNodeBuffer {
    mat4 model;
} node;

layout(location = 0) out vec3 fragPos;
layout(location = 1) out vec3 fragNormal;
layout(location = 2) out vec3 fragColor;
layout(location = 3) out vec2 fragTexCoord;


out gl_PerVertex {
    vec4 gl_Position;
};

void main()
{
	fragPos = (node.model * vec4(inPosition, 1.0)).xyz;
    gl_Position = scene.proj * scene.view * vec4(fragPos, 1.0);
	
	fragColor = inColor;
	fragTexCoord = inTexCoord;
	fragNormal = inNormal;
}



