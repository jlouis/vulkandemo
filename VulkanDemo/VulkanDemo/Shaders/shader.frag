#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 fragPos;
layout(location = 1) in vec3 fragNormal;
layout(location = 2) in vec3 fragColor;
layout(location = 3) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

#define NB_MAX_LIGHTS 8
layout(binding = 2) uniform UniformFragmentSceneBuffer {
    vec4 ambiant;
	vec4 lightPos[NB_MAX_LIGHTS];
	vec4 lightColor[NB_MAX_LIGHTS];
} sceneFragment;

layout(binding = 3) uniform UniformFragmentNodeBuffer {
    float lighting;
} nodeFragment;


layout(binding = 4) uniform sampler2D texSampler;

void main()
{	
	if (nodeFragment.lighting < 0.5)
	{
		outColor = texture(texSampler, fragTexCoord) + vec4(fragColor, 1.0);
	}
	else
	{	
		vec3 normal = normalize(fragNormal);
		vec4 fragmentColor = sceneFragment.ambiant;
		
		for (int i = 0; i < NB_MAX_LIGHTS; i++)
		{
			vec3 lightDir = normalize(sceneFragment.lightPos[i].xyz - fragPos);
			
			float intensity = dot(lightDir, normal);
			
			float distance = length(fragPos - sceneFragment.lightPos[i].xyz);
			intensity /= (distance * 2);
			intensity = clamp(intensity, 0.0f, 1.0f);
			vec4 lightColor = sceneFragment.lightColor[i] * intensity;
			
			fragmentColor += lightColor;
		}		
		
		outColor = (texture(texSampler, fragTexCoord) + vec4(fragColor, 1.0)) * fragmentColor;
	}
}