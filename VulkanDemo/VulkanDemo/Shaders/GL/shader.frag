#version 330 core
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 fragPos;
layout(location = 1) in vec3 fragNormal;
layout(location = 2) in vec3 fragColor;
layout(location = 3) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

#define NB_MAX_LIGHTS 8
uniform vec4 ambiant;
uniform	vec4 lightPos[NB_MAX_LIGHTS];
uniform	vec4 lightColor[NB_MAX_LIGHTS];

uniform float lighting;

uniform sampler2D texSampler;

void main()
{
	if (lighting < 0.5)
	{
		outColor = texture(texSampler, fragTexCoord) + vec4(fragColor, 1.0);
	}
	else
	{
		vec3 normal = normalize(fragNormal);
		vec4 fragmentColor = ambiant;
		
		for (int i = 0; i < NB_MAX_LIGHTS; i++)
		{
			vec3 lightDir = normalize(lightPos[i].xyz - fragPos);
			
			float intensity = dot(lightDir, normal);
			
			float distance = length(fragPos - lightPos[i].xyz);
			intensity /= (distance * 2);
			intensity = clamp(intensity, 0.0f, 1.0f);
			vec4 lightColor = lightColor[i] * intensity;
			
			fragmentColor += lightColor;
		}	
			
		outColor = (texture(texSampler, fragTexCoord) + vec4(fragColor.xyz, 1.0f)) * fragmentColor;
	}
} 