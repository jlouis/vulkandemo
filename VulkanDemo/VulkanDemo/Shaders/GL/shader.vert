#version 330 core
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inColor;
layout(location = 3) in vec2 inTexCoord;

uniform mat4 view;
uniform mat4 proj;
uniform mat4 model;

layout(location = 0) out vec3 fragPos;
layout(location = 1) out vec3 fragNormal;
layout(location = 2) out vec3 fragColor;
layout(location = 3) out vec2 fragTexCoord;


void main()
{
	fragPos = (model * vec4(inPosition, 1.0)).xyz;
    gl_Position = proj * view * vec4(fragPos, 1.0);
	
	fragColor = inColor;
	fragTexCoord = inTexCoord;
	fragNormal = inNormal;
}