#include "AssimpLoader.h"



AssimpLoader::AssimpLoader()
{
}


AssimpLoader::~AssimpLoader()
{
}

glm::vec2 toVec2GLM(aiVector3D vector)
{
	return glm::vec2(vector.x, vector.y);
}

glm::vec3 toVec3GLM(aiVector3D vector)
{
	return glm::vec3(vector.x, vector.y, vector.z);
}

void AssimpLoader::tryToLoadTexture(MeshBuffer* buffer, std::string tex)
{
	if (buffer->_texture)
		return;

	buffer->_texture = new Texture();
	if (!buffer->_texture->load(tex))
	{
		delete buffer->_texture;
		buffer->_texture = nullptr;
	}
}

Mesh* AssimpLoader::loadMesh(std::string filename)
{
	Assimp::Importer importer;
	const aiScene* assimpScene = importer.ReadFile(filename.c_str(), aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_FlipUVs);

	if (!assimpScene)
		return nullptr;

	size_t pos = filename.find_last_of("/\\");
	std::string folderPath = "";
	if (pos != std::string::npos)
		folderPath = filename.substr(0, pos);

	if (folderPath.size() > 0)
	{
		char lastChar = folderPath[folderPath.size() - 1];
		if (lastChar != '/' && lastChar != '\\')
			folderPath.push_back('/');
	}
		
	Mesh* mesh = new Mesh();
	for (unsigned int i = 0; i < assimpScene->mNumMeshes; ++i)
	{
		aiMesh* assimpMesh = assimpScene->mMeshes[i];

		MeshBuffer* buffer = mesh->addMeshBuffer();
		buffer->_vertices.resize(assimpMesh->mNumVertices);
		for (unsigned int i = 0; i < assimpMesh->mNumVertices; ++i)
		{
			buffer->_vertices[i].Position = toVec3GLM(assimpMesh->mVertices[i]);
			buffer->_vertices[i].Color = glm::vec3(0.0f, 0.0f, 0.0f);
			buffer->_vertices[i].Normal = toVec3GLM(assimpMesh->mNormals[i]);

			if (assimpMesh->GetNumUVChannels() > 0)
			{
				const aiVector3D uv = assimpMesh->mTextureCoords[0][i];
				buffer->_vertices[i].UV = toVec2GLM(uv);
			}
		}

		buffer->_indices.resize(assimpMesh->mNumFaces * 3);
		for (unsigned int i = 0; i < assimpMesh->mNumFaces; i++)
		{
			const aiFace face = assimpMesh->mFaces[i];

			if (face.mNumIndices != 3)
			{
				std::cout << "Error : Face != 3 indices !" << std::endl;
				delete mesh;
				return nullptr;
			}

			buffer->_indices[i * 3 + 0] = face.mIndices[0];
			buffer->_indices[i * 3 + 1] = face.mIndices[1];
			buffer->_indices[i * 3 + 2] = face.mIndices[2];
		}
		aiMaterial* mat = assimpScene->mMaterials[assimpMesh->mMaterialIndex];
		if (mat->GetTextureCount(aiTextureType_DIFFUSE) > 0)
		{
			aiString path;
			mat->GetTexture(aiTextureType_DIFFUSE, 0, &path);



			std::cout << "Texture = " << folderPath + path.C_Str() << std::endl;
			tryToLoadTexture(buffer, folderPath + path.C_Str());
		}
	}

	return mesh;
}
