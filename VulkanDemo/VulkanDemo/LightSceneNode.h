#pragma once
#include "SceneNode.h"
class LightSceneNode :
	public SceneNode
{
public:
	LightSceneNode();
	virtual ~LightSceneNode();
	virtual SceneNodeType getNodeType();

	glm::vec4 _color;
};

