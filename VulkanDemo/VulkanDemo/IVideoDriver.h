#pragma once
#include "MeshSceneNode.h"
#include "Mesh.h"
#include "LightSceneNode.h"
#include "Texture.h"

#include <chrono>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>


#define NB_MAX_LIGHTS_PER_NODE 8
// Scene data
struct UniformSceneVertexBuffer
{
	glm::mat4 view;
	glm::mat4 proj;
};

struct UniformSceneFragmentBuffer
{
	glm::vec4 ambiant;
	glm::vec4 lightPos[NB_MAX_LIGHTS_PER_NODE];
	glm::vec4 lightColor[NB_MAX_LIGHTS_PER_NODE];

};

enum DriverType
{
	Driver_Vulkan,
	Driver_OpenGl
};

class IVideoDriver
{
public:
	IVideoDriver();
	~IVideoDriver();

	virtual DriverType getType() = 0;
	
	virtual void prepareScene(glm::vec4 clearColor) = 0;
	virtual void updateSceneUniformBuffer(UniformSceneVertexBuffer& sceneVertexBuffer, UniformSceneFragmentBuffer& sceneFragmentBuffer) = 0;
	virtual void draw(MeshSceneNode * node);
	virtual void endScene();

	virtual void prepareMeshSceneNode(MeshSceneNode* node) = 0;
	virtual void deleteMeshSceneNode(MeshSceneNode* node) = 0;

	virtual void createMeshBuffer(MeshBuffer* buffer) = 0;
	virtual void deleteMeshBuffer(MeshBuffer* buffer) = 0;

	virtual void createTextureBuffer(Texture* texture) = 0;
	virtual bool deleteTextureBuffer(Texture* texture) = 0;

	int getFPS();

private:
	std::chrono::time_point<std::chrono::steady_clock> _last;
	int _fps;
	int _nbFrames;

protected:
	Texture _emptyTex;
};

