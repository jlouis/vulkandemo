#pragma once

#include "MeshBuffer.h"


class Mesh
{
private:
	std::vector<MeshBuffer> _buffers;


public:
	Mesh();
	~Mesh();

	MeshBuffer* addMeshBuffer();
	size_t getMeshBufferCount();
	MeshBuffer* getMeshBuffer(unsigned int i);

	void setDirty(bool dirty);
};

