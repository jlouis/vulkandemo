#pragma once

#include "SceneNode.h"
#include "IVideoDriver.h"
#include <list>

class SceneManager
{
private:
	IVideoDriver* _vk;
	std::list<SceneNode*> _nodes;
	glm::vec3 _ambiantLight;

	void deleteNode(SceneNode * node);
	void updateSceneBuffer();

public:
	SceneManager(IVideoDriver* vk);
	~SceneManager();

	std::vector<SceneNode*> getNodesByType(SceneNodeType type, bool visibleOnly = false);

	MeshSceneNode* addMeshNode(Mesh* mesh = nullptr);
	LightSceneNode* addLightNode();
	void setAmbiantLight(glm::vec3 ambiantLight);
	void removeNode(SceneNode * node);
	void clear();
	void drawAll();
	SceneNode* _camera;
	
};

