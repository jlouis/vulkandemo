#pragma once

#include <vector>
#include <iostream>

#include <assimp\Importer.hpp>
#include <assimp\postprocess.h>
#include <assimp/scene.h>

#include "Mesh.h"



class AssimpLoader
{
	static void tryToLoadTexture(MeshBuffer * buffer, std::string tex);
public:
	AssimpLoader();
	~AssimpLoader();

	static Mesh* loadMesh(std::string filename);
};

