#include "SceneManager.h"
#include <algorithm> 

SceneManager::SceneManager(IVideoDriver * vk) :_vk(vk),
												_camera(new SceneNode()),
												_ambiantLight(glm::vec3(0.0f, 0.0f, 0.0f))
{
}

SceneManager::~SceneManager()
{
	clear();
	delete _camera;
}

std::vector<SceneNode*> SceneManager::getNodesByType(SceneNodeType type, bool visibleOnly)
{
	std::vector<SceneNode*> nodes;
	for (auto node : _nodes)
		if (node->getNodeType() == type)
			if (!visibleOnly || node->isVisible())
				nodes.push_back(node);
	return nodes;
}

MeshSceneNode* SceneManager::addMeshNode(Mesh* mesh)
{
	MeshSceneNode* node = new MeshSceneNode(mesh);
	_vk->prepareMeshSceneNode(node);
	_nodes.push_back(node);
	return node;
}

LightSceneNode* SceneManager::addLightNode()
{
	LightSceneNode* node = new LightSceneNode();
	_nodes.push_back(node);
	return node;
}

void SceneManager::setAmbiantLight(glm::vec3 ambiantLight)
{
	_ambiantLight = ambiantLight;
}

void SceneManager::deleteNode(SceneNode* node)
{
	if (node->getNodeType() == Node_Mesh)
		_vk->deleteMeshSceneNode(static_cast<MeshSceneNode*>(node));
	delete node;
}

void SceneManager::removeNode(SceneNode* node)
{
	_nodes.erase(std::remove(_nodes.begin(), _nodes.end(), node), _nodes.end());
	deleteNode(node);
}

void SceneManager::clear()
{
	for (auto node : _nodes)
		deleteNode(node);

	_nodes.clear();
}

void SceneManager::updateSceneBuffer()
{
	UniformSceneVertexBuffer sceneVertexData;
	sceneVertexData.view = glm::lookAt(_camera->getPosition(), glm::vec3(-1, 0, 0), glm::vec3(0.0f, 1.0f, 0.0f));
	// sceneVertexData.proj is set by the driver

	UniformSceneFragmentBuffer sceneFragmentData;
	sceneFragmentData.ambiant = glm::vec4(_ambiantLight.x, _ambiantLight.y, _ambiantLight.z, 0.0f);

	std::vector<SceneNode*> lights = getNodesByType(Node_Light, true);
	for (unsigned int i = 0; i < NB_MAX_LIGHTS_PER_NODE; ++i)
	{
		if (lights.size() > i)
		{
			LightSceneNode* light = static_cast<LightSceneNode*>(lights[i]);
			sceneFragmentData.lightPos[i] = glm::vec4(light->getPosition().x, light->getPosition().y, light->getPosition().z, 0);
			sceneFragmentData.lightColor[i] = light->_color;
		}
		else
		{
			sceneFragmentData.lightPos[i] = glm::vec4(0, 0, 0, 0);
			sceneFragmentData.lightColor[i] = glm::vec4(0, 0, 0, 0);
		}
	}


	_vk->updateSceneUniformBuffer(sceneVertexData, sceneFragmentData);
}

void SceneManager::drawAll()
{
	// Update scene buffer
	updateSceneBuffer();

	for (auto node : _nodes)
	{
		if (!node->isVisible())
			continue;

		if (node->getNodeType() == Node_Mesh)
			_vk->draw(static_cast<MeshSceneNode*>(node));
	}
}
