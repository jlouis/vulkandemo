#include "IVideoDriver.h"



IVideoDriver::IVideoDriver() : _fps(0), _nbFrames(0)
{
}


IVideoDriver::~IVideoDriver()
{
}

int IVideoDriver::getFPS()
{
	return _fps;
}

void IVideoDriver::endScene()
{
	_nbFrames++;
	auto now = std::chrono::steady_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(now - _last);
	if (duration.count() >= 1000)
	{
		_last = now;
		_fps = _nbFrames;
		_nbFrames = 0;
	}
}

void IVideoDriver::draw(MeshSceneNode* node)
{
	Mesh* mesh = node->getMesh();

	for (size_t i = 0; i < mesh->getMeshBufferCount(); ++i)
	{
		MeshBuffer* buffer = mesh->getMeshBuffer(i);

		if (buffer->_dirty)
			createMeshBuffer(buffer);

		if (!buffer->_texture)
			buffer->_texture = &_emptyTex;

		if (buffer->_texture->_dirty)
			createTextureBuffer(buffer->_texture);
	}
}
