#pragma once

#include "Mesh.h"

enum SceneNodeType
{
	Node_Mesh,
	Node_Light,
	Node_Other
};

class SceneNode
{
	glm::vec3 _position;
	glm::vec3 _rotation;
	glm::vec3 _scale;

	bool _visible;


public:
	SceneNode();
	~SceneNode();

	virtual SceneNodeType getNodeType();

	void setPosition(glm::vec3 position);
	glm::vec3 getPosition();
	void setRotation(glm::vec3 eulerAngles);
	void setScale(glm::vec3 scale);
	void setVisible(bool visible);
	bool isVisible();
	glm::vec3 forward();


	glm::mat4x4 getTransformation();


};

