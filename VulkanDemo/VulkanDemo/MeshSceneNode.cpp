#include "MeshSceneNode.h"



MeshSceneNode::MeshSceneNode(Mesh* mesh) : SceneNode(), _mesh(mesh), _lighting(false)
{
}


MeshSceneNode::~MeshSceneNode()
{
}

SceneNodeType MeshSceneNode::getNodeType()
{
	return Node_Mesh;
}

Mesh * MeshSceneNode::getMesh()
{
	return _mesh;
}
