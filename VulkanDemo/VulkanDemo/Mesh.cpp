#include "Mesh.h"


Mesh::Mesh()
{
}

Mesh::~Mesh()
{
}

MeshBuffer* Mesh::addMeshBuffer()
{
	_buffers.push_back(MeshBuffer());
	return &_buffers[_buffers.size() - 1];
}

size_t Mesh::getMeshBufferCount()
{
	return _buffers.size();
}

MeshBuffer* Mesh::getMeshBuffer(unsigned int i)
{
	if (i >= _buffers.size())
		return nullptr;

	return &_buffers[i];
}

void Mesh::setDirty(bool dirty)
{
	for (MeshBuffer buffer : _buffers)
	{
		buffer._dirty = dirty;
	}

}