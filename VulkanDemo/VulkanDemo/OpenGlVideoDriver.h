#pragma once
#include "CompileConfig.h"

#ifdef COMPILE_WITH_OGL

//#define GLEW_STATIC
#include <GL/glew.h>

#include "IVideoDriver.h"
#include <GLFW/glfw3.h>

#include <vector>
#include <set>
#include <exception>
#include <algorithm>
#include <array>
#include <fstream>
#include <iostream>
#include <chrono>

#include <glm/gtc/type_ptr.hpp>


class OpenGlVideoDriver : public IVideoDriver
{
private:
	GLFWwindow* _window;

	GLuint _shaderProgram;

	unsigned int _windowWidth;
	unsigned int _windowHeight;

	UniformSceneVertexBuffer _vertexSceneBuffer;
	UniformSceneFragmentBuffer _fragmentSceneBuffer;

public:
	OpenGlVideoDriver(GLFWwindow* window, unsigned int windowWidth, unsigned int windowHeight);
	~OpenGlVideoDriver();

	virtual DriverType getType();
	
	virtual void prepareScene(glm::vec4 clearColor);
	virtual void updateSceneUniformBuffer(UniformSceneVertexBuffer& sceneVertexBuffer, UniformSceneFragmentBuffer& sceneFragmentBuffer);
	virtual void draw(MeshSceneNode * node);
	virtual void endScene();

	virtual void prepareMeshSceneNode(MeshSceneNode* node);
	virtual void deleteMeshSceneNode(MeshSceneNode* node);

	virtual void createMeshBuffer(MeshBuffer* buffer);
	virtual void deleteMeshBuffer(MeshBuffer* buffer);

	virtual void createTextureBuffer(Texture* texture);
	virtual bool deleteTextureBuffer(Texture* texture);
};

#endif